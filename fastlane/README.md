fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios beta
```
fastlane ios beta
```

### ios beta_adhoc
```
fastlane ios beta_adhoc
```

### ios get_build_number_for_configuraiton
```
fastlane ios get_build_number_for_configuraiton
```

### ios get_build_version_for_target_and_configurations
```
fastlane ios get_build_version_for_target_and_configurations
```

### ios slack_message
```
fastlane ios slack_message
```

### ios diawi_with_version
```
fastlane ios diawi_with_version
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
