//
//  UILabel+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UILabel {
    func setup(text: String? = nil, size: CGFloat = .fontSize16, weight: UIFont.Weight = .regular, color: UIColor = .white) {
        self.text = text
        let fontSize = .deviceRadio * size
        self.font = UIFont.systemFont(ofSize: fontSize, weight: weight)
        self.textColor = color
    }
}
