//
//  CALayer.swift
//  NimbleTest
//
//  Created by Dev on 9/4/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension CALayer {
    // Common shadow of view
    func applyShadow(color: UIColor = .lcBlack, alpha: Float = 0.28, x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4, spread: CGFloat = 0) {
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
