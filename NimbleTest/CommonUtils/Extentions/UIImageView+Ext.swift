//
//  UIImageView+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UIImageView {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isHidden || !self.isUserInteractionEnabled || self.alpha < 0.01 { return nil }

        let minimumHitArea = CGSize(width: 60, height: 60)
        let buttonSize = self.bounds.size
        let widthToAdd = max(minimumHitArea.width - buttonSize.width, 0)
        let heightToAdd = max(minimumHitArea.height - buttonSize.height, 0)
        let largetFrame = self.bounds.insetBy(dx: -widthToAdd/2, dy: -heightToAdd/2)

        return (largetFrame.contains(point)) ? self : nil
    }
}
