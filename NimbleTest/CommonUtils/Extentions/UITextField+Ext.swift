//
//  UITextField+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UITextField {
    func turnIntoUILabel(borderStyle: UITextField.BorderStyle? = nil, header: Bool = false) {
        self.backgroundColor = UIColor.clear
        if let borderStyle = borderStyle {
            if borderStyle == .none {
                self.borderStyle = .none
                // Add padding.
                let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
                self.leftView = paddingView
                self.leftViewMode = .always
            }
        }
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.red.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        let normalFontSize = .deviceRadio * .textFieldFontSize
        if header {
            let fontSize: CGFloat = Utils.isIPad ? 2.0 * normalFontSize : normalFontSize
            self.font = UIFont.systemFont(ofSize: fontSize)
        }
        else {
            let fontSize = normalFontSize
            self.font = UIFont.systemFont(ofSize: fontSize)
        }
        self.textColor = .lcBlack
    }

    func placeholderAttribute(text: String) {
        let fontSize = .deviceRadio * .textFieldFontSize
        let attributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize)
        ]
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: attributes)
    }

    func lightGreyPlaceholderAttribute(text: String) {
        let attributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: .textFieldFontSize),
            NSAttributedString.Key.foregroundColor: UIColor.lcGrey
        ]
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: attributes)
    }

    func whitePlaceholderAttribute(text: String) {
        let fontSize = .deviceRadio * .textFieldFontSize
        let attribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: attribute)
    }
}
