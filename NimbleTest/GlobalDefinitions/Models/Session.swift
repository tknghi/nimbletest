//
//  Session.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import ObjectMapper

/*
 {
     "data": {
         "id": 565,
         "type": "token",
         "attributes": {
             "access_token": "uH3U90sReytZVC6NUcxm3xCVU1xK-8GBEj0lFjxvakQ",
             "token_type": "Bearer",
             "expires_in": 7200,
             "refresh_token": "HORxv6sjZ3TgacRw6LU-C5x_I7r0s9lGYlkNUJLy0OQ",
             "created_at": 1606673741
         }
     }
 }
 */
class Session: NSObject, Mappable {
    enum Key: String {
        case id             = "data.id"
        case accessToken    = "data.attributes.access_token"
        case tokenType      = "data.attributes.token_type"
        case expiresIn      = "data.attributes.expires_in"
        case refreshToken   = "data.attributes.refresh_token"
        case createdAt      = "data.attributes.created_at"
    }

    var id: String = ""
    var accessToken: String = ""
    var tokenType: String = ""
    var expiresIn: Double = 0
    var refreshToken: String = ""
    var createdAt: Double = 0
    
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        self.id             <- map[Key.id.rawValue]
        self.accessToken    <- map[Key.accessToken.rawValue]
        self.tokenType      <- map[Key.tokenType.rawValue]
        self.expiresIn      <- map[Key.expiresIn.rawValue]
        self.refreshToken   <- map[Key.refreshToken.rawValue]
        self.createdAt      <- map[Key.createdAt.rawValue]
    }
}
