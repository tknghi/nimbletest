//
//  Networking.swift
//  NimbleTest
//
//  Created by Dev on 2020/11/30.
//  Copyright © 2020 Company. All rights reserved.
//

import Foundation
import Moya
import ReactiveSwift
import Result
import SwiftKeychainWrapper
import ObjectMapper

enum NetworkingError: Swift.Error {
    case underlying(Swift.Error)
    case tokenAcquisitionError
    case unknown
}

class OWProvider<Target>: MoyaProvider<Target> where Target: TargetType {
    public init() {
        super.init(endpointClosure: { (target: Target) -> Endpoint in
            let sampleResponseClosure = { return EndpointSampleResponse.networkResponse(200, target.sampleData) }
            let method = target.method
            let task = target.task
            let url = { return target.path.isEmpty ? target.baseURL : target.baseURL.appendingPathComponent(target.path) }
            
            let endpoint = Endpoint(url: url().absoluteString, sampleResponseClosure: sampleResponseClosure, method: method, task: task, httpHeaderFields: [:])
            
            var authorizationString =  ""
            if let accessToken = NBKeychain.accessTokenString, let tokenType = NBKeychain.tokenTypeString {
                authorizationString = tokenType + " " + accessToken
            }
            return endpoint.adding(newHTTPHeaderFields: target.headers(header: authorizationString))
        }, plugins: [NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: NetworkLoggerPlugin.Configuration.LogOptions.verbose))])
    }
}

protocol NetworkingType {
    associatedtype T: TargetType
    var provider: OWProvider<T> { get }
}

struct Networking: NetworkingType {
    typealias T = OWApi
    let provider: OWProvider<OWApi>
}

extension Networking {
    func request(_ token: TargetType, defaults: UserDefaults = UserDefaults.standard) -> SignalProducer<Response, MoyaError> {
        switch token.task {
        case .requestParameters(let parameters, _):
            _ = parameters.reduce(into: String()) { (result, next) in
                if (next.value is String) {
                    result.append("\(next.key) - \(next.value as! String)\n")
                }
            }
        default:
            break
        }
        return SignalProducer { (observer, disposable) in
            let provider = self.provider
            self.handleAPIRequest(withProvider: provider, token: token, observer: observer)
        }
    }
    
    func handleAPIRequest(withProvider provider: OWProvider<OWApi>, token: TargetType, observer: Signal<Response, MoyaError>.Observer) {
        provider.request(token as! OWApi) { result in
            if let value = result.value {
                
                let statusCode = value.response?.statusCode ?? 0
                if (200...299).contains(statusCode) {
                    observer.send(value: value)
                }
                else if [401, 403].contains(statusCode) {
                    switch token as! OWApi {
                    case .login,
                         .resetPassword,
                         .logout:
                        observer.send(error: MoyaError.underlying(NetworkingError.tokenAcquisitionError, value))
                        break
                    default:
                        //Handle refreshhing token here.
                        if let refreshToken = NBKeychain.refreshTokenString {
                            CloudAPIManager.shared.refreshToken.apply((refreshToken, CloudAPIManager.provider)).startWithResult { (result) in
                                switch result {
                                case .success(_):
                                    if let accessToken = NBKeychain.accessTokenString, !accessToken.isEmpty {
                                        //TODO: Optimize this to avoid looping forever and duplicated Refresh token request call
                                        self.handleAPIRequest(withProvider: provider, token: token, observer: observer)
                                        return
                                    }
                                    break
                                case .failure(_):
                                    break
                                }
                                
                                observer.send(error: MoyaError.underlying(NetworkingError.tokenAcquisitionError, value))
                                // End session anyway
                                (UIApplication.shared.delegate as! AppDelegate).endSession()
                            }
                        }
                        else {
                            observer.send(error: MoyaError.underlying(NetworkingError.tokenAcquisitionError, value))
                            // End session anyway
                            (UIApplication.shared.delegate as! AppDelegate).endSession()
                        }
                    }
                }
                else {
                    observer.send(error: MoyaError.statusCode(value))
                }
            }
            else if let error = result.error {
                switch error {
                case .underlying(let error, _):
                    var errorDesription = error.localizedDescription
                    var errorCode: Int = 0
                    /*Error code:
                     -1001: Time out
                     -1009: No internet
                     */
                    if let afError = error.asAFError {
                        switch afError {
                        case .sessionTaskFailed(let _e):
                            errorDesription = _e.localizedDescription
                            errorCode = _e._code
                            break
                        default:
                            break
                        }
                    }
                    
                    //Show alert with abnormal error
                    switch token as! OWApi {
                    default:
                        //Only show general error message when the error is no internet
                        if [-1009, -1004].contains(errorCode) {
                            AppDelegate.shared.window?.rootViewController?.showAlert(message: errorDesription)
                        }
                        break
                    }
                    
                default:
                    break
                }
                
                observer.send(error: error)
            }
            else {
                observer.send(error: MoyaError.underlying(NetworkingError.unknown, nil))
            }
        }
    }
    
    func parse<Output>(response: Response, type: Output.Type) -> Result<Output, NetworkingError> where Output: Mappable {
        guard let value = Mapper<Output>().map(JSONString: response.data.toString()) else {
            return .failure(.underlying(NSError(domain: "OWProvider: Parsing error", code: 0, userInfo: nil)))
        }
        return .success(value)
    }
}

extension NetworkingType {
    static func newDefaultNetworking() -> Networking {
        return Networking(provider: newProvider())
    }
}

private func newProvider<T>() -> OWProvider<T> where T: TargetType {
    return OWProvider()
}


