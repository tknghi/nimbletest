//
//  NBAccountSigninViewModel.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/29/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class NBAccountSigninViewModel: NSObject {
    let id = MutableProperty<String?>(nil)
    let password = MutableProperty<String?>(nil)
    let idValid: Property<Bool>
    let passwordValid: Property<Bool>
    
    var provider: Networking!
    let signIn: Action<(String, String, Networking), Bool, AccountServiceError>
    let loadAccount: Action<(Networking), User, AccountServiceError>
    let signOut: Action<(Networking), Bool, AccountServiceError>
    
    init(debug: Bool = false) {
        let accountService: AccountService
        if (!debug) {
            accountService = AccountServiceImpl()
        } else {
            accountService = AccountServiceDebug()
        }
        
        // TODO: Validate email and password using ReactiveCocoa
        idValid = id.map {
            return ($0 ?? "").count > 0
        }
        
        passwordValid = password.map {
            return ($0 ?? "").count > 0
        }
        
        signIn = Action { input in
            let id = input.0
            let password = input.1
            let provider = input.2
            return accountService.accountSignIn(email: id, password: password, provider: provider)
        }
        
        loadAccount = Action { input in
            return accountService.loadAccount(provider: input)
        }
        
        signOut = Action { input in
            return accountService.accountSignOff(provider: input)
        }
    }
}
