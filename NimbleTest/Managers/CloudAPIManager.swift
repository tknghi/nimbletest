//
//  CloudAPIManager.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 8/12/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ReactiveSwift

class CloudAPIManager: NSObject {
    static let shared = CloudAPIManager.init()
    static let provider = Networking.newDefaultNetworking()
    
    public private(set) var refreshToken: Action<(String, Networking), Bool, AccountServiceError>
    
    override init() {
        let accountService: AccountService = AccountServiceImpl()
        
        // TODO: Optimize this to make sure there is only 1 refreshToken request call at the same time
        
        refreshToken = Action { input in
            return accountService.refreshToken(refreshToken: input.0, provider: input.1)
        }
    }
}
