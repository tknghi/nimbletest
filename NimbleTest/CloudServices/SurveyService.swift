//
//  SurveyService.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import Result
import Moya
import ObjectMapper
import ReactiveSwift

enum SurveyServiceError: Swift.Error {
    case moya(MoyaError)
    case network(NetworkingError)
    case jsonParseError
    case noSurveyFound
    case defaultError(APIError)
    case unknownError
    
    //TODO: Define this function as a extention of Swift.Error
    func defaultError() -> APIError? {
        switch self {
        case .moya(let e):
            switch e {
            case .underlying(_, let errorResponse):
                if let responseData = errorResponse?.data,
                    let apiError = APIError.defaultErrorWithData(responseData){
                    return apiError
                }
            default:
                break
            }
        default:
            break
        }
        return nil
    }
}

protocol SurveyService {
    func getSurvey(pageNumber: Int, pageSize: Int, provider: Networking) -> SignalProducer<([Survey], SurveyMetaData), SurveyServiceError>
}

class SurveyServiceImpl: SurveyService {
    func getSurvey(pageNumber: Int, pageSize: Int, provider: Networking) -> SignalProducer<([Survey], SurveyMetaData), SurveyServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.getSurvey(pageNumber: pageNumber, pageSize: pageSize))
                .mapError(SurveyServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(ifSuccess: { response in
                        do  {
                            if let jsonData = try JSONSerialization.jsonObject(with: response.data, options: .fragmentsAllowed) as? [String : Any],
                               let surveyData = jsonData["data"] as? [[String : Any]],
                               let metaData = jsonData["meta"] as? [String : Int] {
                                if let surveys = Mapper<Survey>().mapArray(JSONObject: surveyData),
                                   let meta = Mapper<SurveyMetaData>().map(JSONObject: metaData) {
                                    sink.send(value:(surveys, meta))
                                }
                                else {
                                    sink.send(error: .jsonParseError)
                                }
                            }
                        }
                        catch (_) {
                            sink.send(error: .jsonParseError)
                        }
                        
                        sink.sendCompleted()
                    }, ifFailure: { failure in
                        var error = failure
                        
                        switch failure {
                        case .moya(let e):
                            switch e {
                            case .underlying(_, let errorResponse):
                                if let statusCode = errorResponse?.response?.statusCode {
                                    if let responseData = errorResponse?.data {
                                        if let apiError = Mapper<APIError>().map(JSONString: responseData.toString()) {
                                            switch apiError.code {
                                            case "invalid_token":
                                                error = .unknownError
                                                break
                                            default:
                                                break
                                            }
                                        }
                                    }
                                }
                            default:
                                break
                            }
                        default:
                            break
                        }
                        
                        sink.send(error: error)
                        sink.sendCompleted()
                    })
            }
        }
    }
}
