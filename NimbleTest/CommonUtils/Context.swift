//
//  Context.swift
//  Company
//
//  Created by Nghi Tran on 7/17/17.
//  Copyright © 2017 Company. All rights reserved.
//

import UIKit

class Context: NSObject {
    class func setCurrentLanguageCode(code : String) -> Void {
        let preferences = UserDefaults.standard
        preferences.set(code,forKey: "LanguageCode")
        preferences.synchronize()
    }
    
    class func getCurrentLanguageCode()-> String {
        if let code = UserDefaults.standard.string(forKey: "LanguageCode") {
            return code
        } else {
            return "en"
        }
    }

}
