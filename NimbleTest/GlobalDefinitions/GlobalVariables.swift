//
//  GlobalVariables.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

struct GlobalVariables {
    static var GLOBAL_DEBUG: Bool = false
    static var BUG_BRANCH_DEBUG: Bool = false
    static var TOKEN_EXPIRY_DEBUG: Bool = false
    static var FIRST_TIME_RUN: Bool = true
    
    static var IS_INTERNAL_BUILD: Bool = true
    #if ADHOC
    static let appDisplayName = Bundle.main.infoDictionary?["CFBundleDisplayName"] ?? ""
    #else
    static let appDisplayName = "Nimble Test"
    #endif
    
    static let logFilePath = FileManager.pathInDocumentsDir(file: "console.log")
    static let logFilePathOld = FileManager.pathInDocumentsDir(file: "console0.log")
    static let supportEmail = "nghi.tran@Companysystems.com"
    
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let surveyStoryboard = UIStoryboard(name: "Survey", bundle: nil)
    
    enum SegueIdentifier: String {
        case surveyList = "SurveyList"
        case surveyDetail = "SurveyDetail"
    }
}
