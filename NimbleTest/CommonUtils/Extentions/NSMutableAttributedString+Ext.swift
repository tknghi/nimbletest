//
//  NSMutableAttributedString+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: .fontSize16),
            NSAttributedString.Key.foregroundColor: UIColor.lcRed
        ]
        let boldString = NSMutableAttributedString(string: text, attributes: attrs)
        self.append(boldString)
        return self
    }

    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let attrs = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: .fontSize16),
            NSAttributedString.Key.foregroundColor: UIColor.lcRed
        ]
        let normal = NSMutableAttributedString(string: text, attributes: attrs)
        self.append(normal)
        return self
    }

    @discardableResult func bold(_ text: String, size: Int) -> NSMutableAttributedString {
        let attrs = [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: CGFloat(size)),
            NSAttributedString.Key.foregroundColor: UIColor.lcBlack
        ]
        let boldString = NSMutableAttributedString(string: text, attributes: attrs)
        self.append(boldString)
        return self
    }
}
