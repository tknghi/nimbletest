//
//  AppDelegate+Log.swift
//  Company
//
//  Created by Developer on 6/15/18.
//  Copyright © 2018 Company. All rights reserved.
//

import UIKit
import MessageUI
import Toast_Swift

fileprivate let MAX_LOG_FILE_SIZE = (5*1024*1024) // 5MB

extension AppDelegate: MFMailComposeViewControllerDelegate {
    
    func createANewAppLog(appLogPath: String) throws {
        let fileManager: FileManager = FileManager.default
        
        if FileManager.sizeOfFile(appLogPath) > MAX_LOG_FILE_SIZE {
            let appLog0 = GlobalVariables.logFilePathOld
            
            if fileManager.fileExists(atPath: appLog0) {
                try fileManager.removeItem(atPath: appLog0)
                NSLog("Remove app log 0 success")
            }
            
            try fileManager.copyItem(atPath: appLogPath, toPath: appLog0)
            NSLog("Copy success");
            try fileManager.removeItem(atPath: appLogPath)
            NSLog("Remove app log success");
            
            #if !DEBUG
            freopen(appLogPath.cString(using: .ascii), "a+", stderr)
            freopen(appLogPath.cString(using: .ascii), "a+", stdout)
            #endif
            
            self.writeSystemInfoLog()
        }
    }
    
    func writeSystemInfoLog() {
        NSLog("System Info Log __________")
        NSLog(Utils.getAppVersionInfo())
        NSLog("Launch time: \(Date().currentTimeZoneDate())")
    }
    
    func sendAppLog() {
        guard MFMailComposeViewController.canSendMail() else {
            self.window?.makeToast(localize("Mail services are not available."))
            return
        }
                
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Get app log
        let logPath = GlobalVariables.logFilePath
        
        //TODO: Zip the data before sending
        var zipData: Data? = nil
        do {
            let logStr = try String(contentsOf: URL(fileURLWithPath: logPath), encoding: .utf8)
            
            //TODO: Encode the data before sending
            //zipData = ZILogEncryptor.encryptString(string: logStr)
            zipData = logStr.data(using: .utf8)
        }
        catch let error {
            NSLog("Read zip log file error \(error.localizedDescription)")
        }
        
        if zipData != nil {
            composeVC.addAttachmentData(zipData!,
                                        mimeType:"text/plain",
                                        fileName:"ios_application.\(zipData!.count/1024)KB.log")
        }
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([GlobalVariables.supportEmail])
        composeVC.setSubject(Utils.getAppVersionInfo())
        composeVC.setMessageBody("\(NBKeychain.user?.email != nil ? "Username: \(NBKeychain.user!.email!)\n" : "")Resolving your issue is important to us. Please explain your issue in the field below.\n\n\n\n\n\n", isHTML: false)
        // Present the view controller modally.
        self.window?.rootViewController?.present(composeVC, animated: true, completion: nil)
    }
    
    // MARK:- MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult,
                               error: Error?) {
        NSLog("Send app log error %@", error?.localizedDescription ?? "")
        
        let weakSelf = self
        
        // Check the result or perform other tasks.
        switch result {
        case .saved:
            weakSelf.window?.makeToast(localize("App log saved"))
        case .sent:
            weakSelf.window?.makeToast(localize("App log sent"))
        case .cancelled:
            weakSelf.window?.makeToast(localize("Cancelled sending app log"))
        case .failed:
            weakSelf.window?.makeToast(localize("Failed to send app log"))
        @unknown default:
            break
        }
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}
