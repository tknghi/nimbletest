//
//  UINavigationController+Ext.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 9/17/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UINavigationController {
    func popToFirstViewController<T>(vc: T.Type, animated: Bool = true) {
        guard let firstVC = viewControllers.first(where: {type(of: $0) == vc}) else { return }
        self.popToViewController(firstVC, animated: animated)
    }
    
    func popToLastViewController<T>(vc: T.Type, animated: Bool = true) {
        guard let lastVC = viewControllers.last(where: {type(of: $0) == vc}) else { return }
        self.popToViewController(lastVC, animated: animated)
    }
}
