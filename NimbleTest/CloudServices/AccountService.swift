//
//  AccountService.swift
//  NimbleTest
//
//  Created by dev on 2017/06/16.
//  Copyright © 2020 Company. All rights reserved.
//

import ReactiveSwift
import Result
import Moya
import SwiftKeychainWrapper
import ObjectMapper

enum AccountServiceError: Swift.Error {
    case moya(MoyaError)
    case network(NetworkingError)
    case jsonParseError
    case noAccountFound
    case cannotGetAccountInfo
    case editAccountFailed
    case loginFailed
    case userBlocked
    case userSuspended
    case emailNotVerified
    case forgotPasswordError(code: Int)
    case tokenExpired
    case defaultError(APIError)
    case unknownError
    
    //TODO: Define this function as a extention of Swift.Error
    func defaultError() -> APIError? {
        switch self {
        case .moya(let e):
            switch e {
            case .underlying(_, let errorResponse):
                if let responseData = errorResponse?.data,
                    let apiError = APIError.defaultErrorWithData(responseData){
                    return apiError
                }
            default:
                break
            }
        default:
            break
        }
        return nil
    }
}

protocol AccountService {
    
    func checkSignIn(provider: Networking) -> SignalProducer<String, AccountServiceError>
    
    func accountSignIn(email: String, password: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError>
    
    func loadAccount(provider: Networking) -> SignalProducer<User, AccountServiceError>
    
    func forgotPassword(email: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError>
    
    func accountSignOff(provider: Networking) -> SignalProducer<Bool, AccountServiceError>
    
    func refreshToken(refreshToken: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError>
}

class AccountServiceImpl: AccountService {
    func checkSignIn(provider: Networking) -> SignalProducer<String, AccountServiceError> {
        if let user = NBKeychain.user, user.id.count > 0, NBKeychain.accessTokenString != nil {
            return SignalProducer { (sink, disposable) -> () in
                provider.request(OWApi.getAccount(userId: user.id))
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(ifSuccess: { response in
                        if let user = Mapper<User>().map(JSONString: response.data.toString()) {
                            NBKeychain.user = user
                            sink.send(value: "HomeView")
                            sink.sendCompleted()
                        }
                        else {
                            sink.send(error: AccountServiceError.cannotGetAccountInfo)
                            sink.sendCompleted()
                        }
                    }, ifFailure: { failure in
                        sink.send(error: AccountServiceError.cannotGetAccountInfo)
                        sink.sendCompleted()
                    })
                }
            }
        }
        else {
            return SignalProducer(value: "SignInView")
        }
    }
    /*
     This function called when user try to signed in.
     Observe for Login JSON values. If authenticated, save to Keychain, retrieve user info from Keychain.
     If cannot login, return immediately.
     If cannot retrieve user info from CoreData, fetch user info from API and then save the result into CoreDat
    */
    func accountSignIn(email: String, password: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.login(email: email, password: password))
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(ifSuccess: { response in
                        if let session = Mapper<Session>().map(JSONString: response.data.toString()) {
                            
                            NBKeychain.accessTokenString = session.accessToken
                            NBKeychain.refreshTokenString = session.refreshToken
                            NBKeychain.tokenTypeString = session.tokenType
                            
                            sink.send(value: true)
                        }
                        else {
                            sink.send(error: .jsonParseError)
                        }
                        sink.sendCompleted()
                    }, ifFailure: { failure in
                        var loginError = failure
                        
                        switch failure {
                        case .moya(let e):
                            switch e {
                            case .underlying(_, let errorResponse):
                                if let statusCode = errorResponse?.response?.statusCode {
                                    if statusCode == 401, let responseData = errorResponse?.data {
                                        if let apiError = Mapper<APIError>().map(JSONString: responseData.toString()) {
                                            switch apiError.code {
                                            case "LOGIN_FAILED_EMAIL_NOT_VERIFIED":
                                                loginError = .emailNotVerified
                                                break
                                            case "LOGIN_FAILED":
                                                loginError = .loginFailed
                                            case "USER_BLOCKED":
                                                loginError = .userBlocked
                                                break
                                            case "USER_SUSPENDED":
                                                loginError = .userSuspended
                                            default:
                                                break
                                            }
                                        }
                                    }
                                }
                            default:
                                break
                            }
                        default:
                            break
                        }
                        
                        sink.send(error: loginError)
                        sink.sendCompleted()
                    })
            }
        }
    }
    
    func refreshToken(refreshToken: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.refreshToken(refreshToken: refreshToken))
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(ifSuccess: { response in
                        if let session = Mapper<Session>().map(JSONString: response.data.toString()) {
                            
                            NBKeychain.accessTokenString = session.accessToken
                            NBKeychain.refreshTokenString = session.refreshToken
                            NBKeychain.tokenTypeString = session.tokenType
                            
                            sink.send(value: true)
                        }
                        else {
                            sink.send(error: .jsonParseError)
                        }
                        sink.sendCompleted()
                    }, ifFailure: { failure in
                        sink.send(error: failure)
                        sink.sendCompleted()
                    })
            }
        }
    }
    
    /*
     This function is called when user tries to sign off.
    */
    func accountLogout(provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.logout)
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(
                        ifSuccess: { resp in
                            if resp.statusCode == 200 || resp.statusCode == 204 {
                                sink.send(value: true)
                            }
                            else if resp.statusCode == 400 {
                                sink.send(value: false)
                            }
                            else {
                                sink.send(error: .unknownError)
                            }
                        },
                        ifFailure: { failure in
                            switch failure {
                            case .moya(let e):
                                let statusCode = e.response?.statusCode
                                if statusCode == 204 {
                                    sink.send(value: true)
                                }
                                else {
                                    sink.send(error: failure)
                                }
                            default:
                                sink.send(error: failure)
                            }
                        }
                    )
                    sink.sendCompleted()
                }
        }
    }
    
    func loadAccount(provider: Networking) -> SignalProducer<User, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            guard let accessToken = NBKeychain.accessTokenString, accessToken.count > 0 else {
                sink.send(error: .noAccountFound)
                sink.sendCompleted()
                return
            }
            
            provider.request(OWApi.getAccount(userId: accessToken))
            .mapError(AccountServiceError.moya)
            .startWithResult { result -> () in
                result.analysis(ifSuccess: { response in
                    if let user = Mapper<User>().map(JSONString: response.data.toString()), !user.id.isEmpty {
                        NBKeychain.user = user
                        sink.send(value: user)
                    }
                    else if let apiError = Mapper<APIError>().map(JSONString: response.data.toString()), !apiError.code.isEmpty {
                        sink.send(error: .defaultError(apiError))
                    }
                    else {
                        sink.send(error: .cannotGetAccountInfo)
                    }
                    sink.sendCompleted()
                }, ifFailure: { failure in
                    sink.send(error: failure)
                    sink.sendCompleted()
                })
            }
        }
    }
    
    func forgotPassword(email: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.resetPassword(email: email))
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(ifSuccess: { reset in
                        if reset.statusCode == Int(204) {
                            sink.send(value: true)
                        } else {
                            sink.send(error: .forgotPasswordError(code: reset.statusCode))
                        }
                        sink.sendCompleted()
                    }, ifFailure: { failure in
                        switch failure {
                        case .moya(let e):
                            
                            //TODO: Define general API Error object to parse error data
                            switch e {
                            case .underlying(_, let errorResponse):
                                if let statusCode = errorResponse?.response?.statusCode {
                                    if (statusCode == 400) {
                                        sink.send(value: false)
                                    }
                                    else if statusCode == 401, let responseData = errorResponse?.data {
                                        if let apiError = Mapper<APIError>().map(JSONString: responseData.toString()) {
                                            switch apiError.code {
                                            case "RESET_FAILED_EMAIL_NOT_VERIFIED":
                                                sink.send(error: .emailNotVerified)
                                                break
                                            default:
                                                break
                                            }
                                        }
                                    }
                                }
                                else {
                                    sink.send(error: failure)
                                }
                            default:
                                sink.send(error: failure)
                            }
                        default:
                            sink.send(error: failure)
                        }
                        
                        sink.sendCompleted()
                    })
            }
        }
    }
    
    func accountSignOff(provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        
        return SignalProducer { (sink, disposable) -> () in
            provider.request(OWApi.logout)
                .mapError(AccountServiceError.moya)
                .startWithResult { result -> () in
                    result.analysis(
                        ifSuccess: { resp in
                            if resp.statusCode == 200 || resp.statusCode == 204 {
                                sink.send(value: true)
                            }
                            else if resp.statusCode == 400 {
                                sink.send(value: false)
                            }
                            else {
                                sink.send(error: .unknownError)
                            }
                        },
                        ifFailure: { failure in
                            switch failure {
                            case .moya(let e):
                                let statusCode = e.response?.statusCode
                                if statusCode == 204 {
                                    sink.send(value: true)
                                }
                                else {
                                    sink.send(error: failure)
                                }
                            default:
                                sink.send(error: failure)
                            }
                        }
                    )
                    sink.sendCompleted()
                }
        }
    }
}

