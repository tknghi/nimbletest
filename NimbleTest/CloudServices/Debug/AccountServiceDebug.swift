//
//  AccountServiceDebug.swift
//  NimbleTest
//
//  Created by user on 10/18/18.
//  Copyright © 2020 Company. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result
import Moya

class AccountServiceDebug: AccountService {
    
    func refreshToken(refreshToken: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) -> () in
            
        }
    }
    
    func markNotificationsAsRead(engine: [UInt64], provider: Networking) -> SignalProducer<Void, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
    
    func checkSignIn(provider: Networking) -> SignalProducer<String, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
    
    func accountSignIn(email: String, password: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
    
    func loadAccount(provider: Networking) -> SignalProducer<User, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
    
    func forgotPassword(email: String, provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
    
    func accountSignOff(provider: Networking) -> SignalProducer<Bool, AccountServiceError> {
        return SignalProducer { (sink, disposable) in
            
        }
    }
}
