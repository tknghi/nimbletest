//
//  LCSigninView.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/31/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

class NBSigninView: BaseView {

    // MARK: - IBOutlet
    @IBOutlet weak var idTextField: NBTextInput!
    @IBOutlet weak var idTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: NBTextInput!
    @IBOutlet weak var passwordTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var signInButton: NBButton!

    // MARK: - Public
    var didEnterIdAndPassword: ((_ id: String, _ password: String) -> Void)?

    // MARK: - Override
    override func setupView() {
        super.setupView()
        contentView.layer.cornerRadius = 12.0
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        
        layer.shadowColor = UIColor.lcBlack10.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 0, height: 24)
        layer.shadowRadius = 12
        
        idTextField.titleLabel.setup(text: "")
        //idTextField.textField.placeholder = localize("Email")
        idTextField.textField.textContentType = .emailAddress
        idTextField.textField.keyboardType = .emailAddress
        idTextField.delegate = self
        idTextField.textField.backgroundColor = .color(r: 255, g: 255, b: 255, alpha: 0.1)
        idTextField.textField.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.color(r: 255, g: 255, b: 255, alpha: 0.8)])

        
        passwordTextField.titleLabel.setup(text: "")
        //passwordTextField.textField.placeholder = localize("Password")
        passwordTextField.isPasswordField = true
        passwordTextField.textField.isSecureTextEntry = true
        passwordTextField.delegate = self
        passwordTextField.textField.backgroundColor = .color(r: 255, g: 255, b: 255, alpha: 0.1)
        passwordTextField.textField.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.color(r: 255, g: 255, b: 255, alpha: 0.8)])
        
        signInButton.renderButton(forStype: .positive, text: localize("Log in"))
        signInButton.isEnabled = false
    }

    // MARK: - IBAction
    @IBAction func signInButtonTapped(_ sender: Any) {
        didEnterIdAndPassword?(idTextField.text, passwordTextField.text)
    }

    @IBAction func backgroundTapToDimiss(_ sender: Any) {
        endEditing(true)
    }
    
    /// Call to clear all fields on the view
    func reset() {
        idTextField.reset()
        passwordTextField.reset()
        signInButton.isEnabled = false
    }
}

// MARK: - NBTextInputDelegate
extension NBSigninView: NBTextInputDelegate {
    func textFieldDidBeginEditing(_ textField: NBTextInput) {

    }

    func textFieldDidEndEditing(_ textField: NBTextInput) {
        if textField == idTextField {
            if textField.text.isEmpty {
                textField.errorText = localize("Email can’t be blank.")
            }
            else if !textField.text.isValidEmail() {
                textField.errorText = localize("Please enter a correct email.")
            }
        }
        else {
            if textField.text.isEmpty {
                textField.errorText = localize("Password can’t be blank.")
            }
        }
    }
    
    func textField(_ textField: NBTextInput, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text
        guard let stringRange = Range(range, in: currentText) else {
            return false
        }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        var shouldEnableSignin = false
        
        if textField == idTextField {
            shouldEnableSignin = !updatedText.isEmpty
                && updatedText.isValidEmail()
                && !passwordTextField.text.isEmpty
            if updatedText.count > NBAppStyle.TextLength.userId {
                return false
            }
        } else if textField == passwordTextField {
            shouldEnableSignin = !updatedText.isEmpty && !idTextField.text.isEmpty
            if updatedText.count > NBAppStyle.TextLength.password {
                return false
            }
        }
        signInButton.isEnabled = shouldEnableSignin
        return true
    }
    
    func textFieldShouldClear(_ textField: NBTextInput) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: NBTextInput) -> Bool {
        if textField == passwordTextField {
            endEditing(true)
        }
        return true
    }
}
