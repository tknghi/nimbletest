//
//  LCButton.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

class NBButton: UIButton {
    
    enum LCButtonStyle: String {
        case positive = "Positive"
        case negative = "Negative"
        case statndardLinkWhite = "StandardLinkWhite"
        case standardLink = "StandardLink"
        case standardLinkGrey = "StandardLinkGrey"
        case checkBox = "CheckBox"
    }

    var style: LCButtonStyle = .positive
    override var isEnabled: Bool {
        didSet {
            switch style {
            case .positive:
                break
            case .negative:
                layer.borderColor = isEnabled ? UIColor.lcBlack20.cgColor : UIColor.lcBlack10.cgColor
                break
            case .statndardLinkWhite:
                break
            case .standardLink:
                break
            case .standardLinkGrey:
                break
            case .checkBox:
                break
            }
        }
    }

    func renderButton(forStype style: LCButtonStyle, text: String = String.emptyString, icon: UIImage? = nil) {
        self.style = style
        let standardFontSize: CGFloat = .fontSize16 * .deviceRadio
        self.titleLabel?.font = UIFont.systemFont(ofSize: standardFontSize, weight: .bold)
        self.setTitle(text, for: .normal)
        self.backgroundColor = .clear
        switch (style) {
        case .positive:
            self.backgroundColor = .white
            self.setTitleColor(.black, for: .normal)
            self.setTitleColor(.black, for: .highlighted)
            self.setTitleColor(.black, for: .disabled)

            if let icon = icon {
                self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 4)
                self.setImage(icon, for: .normal)
                self.setImage(icon, for: .highlighted)
                self.setImage(icon, for: .disabled)
            } else {
                self.imageEdgeInsets = .zero
            }

            self.layer.cornerRadius = 10
        case .negative:
            self.setBackgroundImage(Utils.imageWithColor(.white), for: .normal)
            self.setBackgroundImage(Utils.imageWithColor(.lcBlack08), for: .highlighted)
            self.setBackgroundImage(Utils.imageWithColor(.white), for: .disabled)

            self.setTitleColor(.lcBlack50, for: .normal)
            self.setTitleColor(.lcBlack50, for: .highlighted)
            self.setTitleColor(.lcBlack20, for: .disabled)

            self.layer.borderWidth = 1.0
            self.layer.borderColor = UIColor.lcBlack20.cgColor
            
            self.layer.cornerRadius = 10
        case .standardLinkGrey:
            self.titleLabel?.font = UIFont.systemFont(ofSize: .fontSize14 * .deviceRadio)
            self.setAttributedTitle(underlineTextAttribute(text: text, color: .lcBlack70), for: .normal)
            self.setTitleColor(.darkGray, for: .normal)
        case .statndardLinkWhite:
            self.titleLabel?.font = UIFont.systemFont(ofSize: .fontSize14 * .deviceRadio)
            self.setAttributedTitle(underlineTextAttribute(text: text, color: .black), for: .normal)
        case .checkBox:
            self.setTitleColor(.lightGray, for: .normal)
        default:
            self.setAttributedTitle(underlineTextAttribute(text: text, color: .lcBlack70), for: .normal)
        }
        self.clipsToBounds = true
    }

    fileprivate func underlineTextAttribute(text: String, color: UIColor) -> NSMutableAttributedString {
        let titleString = NSMutableAttributedString(string: text)
        titleString.addAttributes([
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.foregroundColor: color
        ], range: NSMakeRange(0, text.count))
        return titleString
    }
}
