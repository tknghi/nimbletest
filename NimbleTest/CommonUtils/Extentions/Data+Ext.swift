//
//  Data+Ext.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 8/19/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension Data {
    func toString(encoding: String.Encoding = .utf8) -> String {
        return String.init(data: self, encoding: encoding) ?? ""
    }
}
