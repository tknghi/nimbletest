//
//  NBSurveyDetailViewController.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit

class NBSurveyDetailViewController: CommonViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - IBAction
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
