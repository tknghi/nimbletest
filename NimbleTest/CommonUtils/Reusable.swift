//
//  Reusable.swift
//  NimbleTest
//
//  Created by Dev on 9/7/20.
//  Copyright © 2020 Company. All rights reserved.
//

/// Make `UITableViewCell` and `UICollectionViewCell` subclasses
/// conform to this protocol when they are only code-based
/// to be able to dequeue them in a type-safe manner
protocol Reusable: class {
    /// The reuse identifier to use when registering and later dequeuing a reusable cell
    static var reuseIdentifier: String { get }
}

extension Reusable {
    /// By default, use the name of the class as String for its reuseIdentifier
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
