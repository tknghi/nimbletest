//
//  UIColor+Ext.swift
//  Company
//
//  Created by Nghi Tran Inc on 7/7/16.
//  Copyright © 2017 Company. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    // The color definition on this file is defined in the Zeplin document.
    // #31C720
    static var lcGreen = color(r: 49, g: 199, b: 32, alpha: 1)
    // #31C720
    static var lcDarkGreen = color(r: 48, g: 168, b: 34, alpha: 1)
    // #02AEEE
    static var lcBlue = color(r: 2, g: 174, b: 238, alpha: 1)
    // #7847E2
    static var lcPurple = color(r: 120, g: 71, b: 226, alpha: 1)
    // #8F95B3
    static var lcGrey = color(r: 143, g: 149, b: 179, alpha: 1)
    // #F5222D
    static var lcRed = color(r: 245, g: 34, b: 45, alpha: 1)
    // #FF8F00
    static var lcOrange = color(r: 255, g: 143, b: 0, alpha: 1)
    // #2C2C2C
    static var lcBlack = color(r: 44, g: 44, b: 44, alpha: 1)
    // #2C2C2C-70
    static var lcBlack70 = color(r: 44, g: 44, b: 44, alpha: 0.7)
    // #2C2C2C-50
    static var lcBlack50 = color(r: 44, g: 44, b: 44, alpha: 0.5)
    // #2C2C2C-30
    static var lcBlack30 = color(r: 44, g: 44, b: 44, alpha: 0.3)
    // #2C2C2C-20
    static var lcBlack20 = color(r: 44, g: 44, b: 44, alpha: 0.2)
    // #2C2C2C-10
    static var lcBlack10 = color(r: 44, g: 44, b: 44, alpha: 0.1)
    // #2C2C2C-8
    static var lcBlack08 = color(r: 44, g: 44, b: 44, alpha: 0.08)
    //
    static var lcBlack80 = color(r: 44, g: 44, b: 44, alpha: 0.8)
    //
    static var lcBlack90 = color(r: 44, g: 44, b: 44, alpha: 0.9)
    // #F5F6FA
    static var lcBackground = color(r: 245, g: 246, b: 250, alpha: 1)
    // #2B333A
    static var lcGrayBlack = color(r: 43, g: 51, b: 58, alpha: 1)

    static let connectedColor = UIColor.lightGray.toColor(.lcGreen, percentage: 50)
    static let offlineColor = UIColor.lightGray.toColor(.lcOrange, percentage: 50)
    static let highlightTitleColor: UIColor = .lcGreen
    static let titleColor: UIColor = .lcBlack
    static let separatorColor: UIColor = .lcBlack08

    // MARK: - Utilities
    func toColor(_ color: UIColor, percentage: CGFloat) -> UIColor {
        let percentage = max(min(percentage, 100), 0) / 100
        switch percentage {
        case 0: return self
        case 1: return color
        default:
            var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
            var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
            guard self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1) else { return self }
            guard color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2) else { return self }

            return UIColor(red: CGFloat(r1 + (r2 - r1) * percentage),
                           green: CGFloat(g1 + (g2 - g1) * percentage),
                           blue: CGFloat(b1 + (b2 - b1) * percentage),
                           alpha: CGFloat(a1 + (a2 - a1) * percentage))
        }
    }
}

extension UIColor {
    convenience init(red: CGFloat, green: CGFloat, blue: CGFloat, andAlpha alpha: CGFloat) {
        self.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    static func colorWithHexString (hex:String, alpha: CGFloat = 1.0) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(index: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = cString.substringToIndex(index: 2)
        let gString = cString.substringFromIndex(index: 2).substringToIndex(index: 2)
        let bString = cString.substringFromIndex(index: 4).substringToIndex(index: 2)
        
        var r: Int = 0, g: Int = 0, b: Int = 0
        Scanner.init(string: rString).scanInt(&r)
        Scanner.init(string: gString).scanInt(&g)
        Scanner.init(string: bString).scanInt(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    /**
     amount greater than 1 is darker, less than 1 is lighter
     */
    func adjustBrightness(_ amount:CGFloat) -> UIColor {
        var hue:CGFloat = 0
        var saturation:CGFloat = 0
        var brightness:CGFloat = 0
        var alpha:CGFloat = 0
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            brightness += (amount-1.0)
            brightness = max(min(brightness, 1.0), 0.0)
            return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        }
        return self
    }
    
    static func color(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: alpha)
    }
    
    static func color(value: CGFloat) -> UIColor {
        return UIColor(red: value / 255, green: value / 255, blue: value / 255, alpha: 1)
    }
    
    static func color(hex: String, alpha: CGFloat = -1) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var _alpha: Int = 0
        if alpha == -1 {
            _alpha = 1
        }
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count == 8 {
            let alphaString = cString[cString.startIndex.hashValue...2]
            cString = cString[cString.endIndex.hashValue...(-6)]
            
            if alpha == -1 {
                var al: Int = 0
                Scanner.init(string: alphaString).scanInt(&al)
                _alpha = al
            }
        }
        
        if ((cString.count) != 6 && (cString.count) != 8) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(_alpha)
        )
    }
    
    func rgbToInt() -> Int? {
        guard let rbg = self.rgb() else {
            return nil
        }
        
        let rgbInt = (rbg.al << 24) + (rbg.r << 16) + (rbg.g << 8) + rbg.b
        return rgbInt
    }
    
    func rgb() -> (r: Int, g: Int, b: Int, al: Int)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return (r: iRed, g: iGreen, b: iBlue, al: iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
    
    func rgbToSendCloud() -> UInt32? {
        guard let rbg = self.rgb() else {
            return nil
        }
        
        return UInt32(256*256*rbg.r + 256*rbg.g + rbg.b)
    }
    
    convenience init(hex8: UInt32) {
        let divisor = CGFloat(255)
        let alpha   = CGFloat((hex8 & 0xFF000000) >> 24) / divisor
        let red     = CGFloat((hex8 & 0x00FF0000) >> 16) / divisor
        let green   = CGFloat((hex8 & 0x0000FF00) >>  8) / divisor
        let blue    = CGFloat( hex8 & 0x000000FF       ) / divisor
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}
