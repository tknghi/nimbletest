//
//  UINavigationBar.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func setBackgroundColor(_ color: UIColor) {
        self.setBackgroundImage(Utils.imageWithColor(color), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
}
