//
//  NBSurveyViewController.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class NBSurveyViewController: CommonViewController {

    // MARK: - Private UIs
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var todayLabel: UILabel!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var surveyCollectionView: UICollectionView!
    
    // MARK: - Private Properties
    private let surveysViewModel = NBSurveyListViewModel()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSurvey()
    }
    
    override func setupView() {
        super.setupView()
        
        if #available(iOS 11.0, *) {
            surveyCollectionView.contentInsetAdjustmentBehavior = .never
            if #available(iOS 13.0, *) {
                surveyCollectionView.automaticallyAdjustsScrollIndicatorInsets = false
            }
        }
        else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        
        timeLabel.setup(text: Date().toString("EEEE, MMMM dd"), size: .fontSize16, weight: .semibold)
        timeLabel.numberOfLines = 0
        todayLabel.setup(text: localize("Today"), size: .fontSize24, weight: .semibold)
        
        pageControl.numberOfPages = 0
        surveyCollectionView.dataSource = self
        surveyCollectionView.delegate = self
        surveyCollectionView.register(UINib(nibName: "NBSurveyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NBSurveyCollectionViewCell")
    }
    
    override func bindViewModel() {
        disposables += surveysViewModel.surveys.signal.take(during: reactive.lifetime)
            .observeValues({ [weak self] surveys in
                guard let strongSelf = self, surveys.count > 0 else { return }
                ActivityIndicator.stop()
                
                //Display new surveys list
                strongSelf.updateUI()
            })
        
        disposables += surveysViewModel.displayingSurvey.signal.take(during: reactive.lifetime).observeResult({ [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let survey):
                guard let _survey = survey,
                      let index = strongSelf.surveysViewModel.surveys.value.firstIndex(of: _survey),
                      index < strongSelf.pageControl.numberOfPages else { return }
                
                strongSelf.pageControl.currentPage = index
                break
            }
        })
        
        disposables += surveysViewModel.currentSurvey.signal.take(during: reactive.lifetime).observeResult({ [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let survey):
                guard let _survey = survey else { return }
                strongSelf.showSurveyDetail(survey: _survey)
                break
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        surveyCollectionView.reloadData()
    }
    
    deinit {
        disposables.dispose()
    }
}

extension NBSurveyViewController {
    func loadSurvey() {
        ActivityIndicator.start("Loading surveys")
        surveysViewModel.loadServeys()
    }
    
    func loadMoreSurvey() {
        if surveysViewModel.loadMoreServeys() {
            //Consider to show indicator while loading more survey or not. This may annoy users
            //ActivityIndicator.start("")
        }
    }
    
    func updateUI() {
        pageControl.numberOfPages = surveysViewModel.surveys.value.count
        surveyCollectionView.reloadData()
    }
    
    // MARK: - Utilities
    func showSurveyDetail(survey: Survey) {
        
        //TODO: Paste survey detail object to the detail view here
        
        self.navigationController?.navigationBar.isHidden = false
        performSegue(withIdentifier: GlobalVariables.SegueIdentifier.surveyDetail.rawValue, sender: self)
    }
}

extension NBSurveyViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return surveysViewModel.surveys.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NBSurveyCollectionViewCell", for: indexPath) as! NBSurveyCollectionViewCell
        
        let survey = surveysViewModel.surveys.value[indexPath.row]
        cell.displaySurvey(survey: survey)
        cell.surveysViewModel = surveysViewModel
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth, height: screenHeight - 1)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        notifyPageChangedIfNeeded(isTabbarDidAppear: true)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        notifyPageChangedIfNeeded(isTabbarDidAppear: false)
    }

    func notifyPageChangedIfNeeded(isTabbarDidAppear: Bool) {
        let pageWidth = surveyCollectionView.bounds.width
        let page = Int(surveyCollectionView.contentOffset.x / pageWidth)
        guard page >= 0, page < surveysViewModel.maxNumberOfSurveys else { return }
        pageControl.currentPage = page
        
        guard page < surveysViewModel.maxNumberOfSurveys else { return }
        let survey = surveysViewModel.surveys.value[page]
        surveysViewModel.displayingSurvey.value = survey
        
        //Check to handle loading more survey
        if page >= surveysViewModel.surveys.value.count - 2 {
            loadMoreSurvey()
        }
    }
}
