//
//  UIImage.swift
//  NimbleTest
//
//  Created by Dev on 8/24/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UIImage {
    static let icAddWhite           = UIImage(named: "ic-add-white")
    static let icFlashOn            = UIImage(named: "ic-flash-on")
    static let icFlashOff           = UIImage(named: "ic-flash-off")
    static let icNextWhite          = UIImage(named: "ic-next-white")
    static let icNextGray           = UIImage(named: "ic-next-gray")
    static let icCloseWhite         = UIImage(named: "ic-close-white")
    static let icCloseGray          = UIImage(named: "ic-close-gray")
    static let icArrowLeft          = UIImage(named: "ic-arrow-left")
    static let icTrash              = UIImage(named: "ic-trash")
    static let icCheckboxUnselected = UIImage(named: "ic-checkbox-unselected")
    static let icCheckboxSelected   = UIImage(named: "ic-checkbox-selected")
    static let icDropDown           = UIImage(named: "ic-drop-down")
    static let icDropUp             = UIImage(named: "ic-drop-up")
    static let icMotionTest         = UIImage(named: "ic-motion-test")
}

extension UIImage {
    func sizeOfAttributeString(str: NSAttributedString, maxWidth: CGFloat) -> CGSize {
        let size = str.boundingRect(with: CGSize.init(width: maxWidth, height: 1000), options: (NSStringDrawingOptions.usesLineFragmentOrigin), context: nil).size
        return size
    }

    func imageFromText(text: NSString, size: CGFloat = 14) -> UIImage {
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraph.alignment = .center
        let fontSize = Utils.isIPad ? size * CGFloat.deviceRadio : size
        let font = UIFont.systemFont(ofSize: fontSize)

        let attributedString = NSAttributedString(string: text as String, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.lcBlack, NSAttributedString.Key.paragraphStyle: paragraph])

        let size = sizeOfAttributeString(str: attributedString, maxWidth: CGFloat(45))
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        attributedString.draw(in: CGRect(x:0, y:0, width:size.width, height:size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
