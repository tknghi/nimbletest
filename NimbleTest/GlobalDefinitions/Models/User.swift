//
//  User.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    enum Key: String {
        case id = "data.id"
        case email = "data.attributes.email"
        case avatarUrl = "data.attributes.avatar_url"
    }
    
    var id: String = ""
    var email: String?
    var avatarUrl: String?
    
    
    // MARK: Object Mapper
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.id <- map[Key.id.rawValue]
        self.email <- map[Key.email.rawValue]
        self.avatarUrl <- map[Key.avatarUrl.rawValue]
    }
    
    init(id: String = "", email: String = "", avatarUrl: String = "") {
        self.id = id
        self.email = email
        self.avatarUrl = avatarUrl
    }
    
    var printMe : String {
        get {
            return "\(String(describing: id)) \(String(describing: email))"
        }
    }
    
    //MARK: - NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: Key.id.rawValue)
        coder.encode(email, forKey: Key.email.rawValue)
        coder.encode(avatarUrl, forKey: Key.avatarUrl.rawValue)
    }

    required init?(coder: NSCoder) {
        super.init()
        self.id = (coder.decodeObject(forKey: Key.id.rawValue) as? String) ?? ""
        self.email = (coder.decodeObject(forKey: Key.email.rawValue) as? String) ?? ""
        self.avatarUrl = (coder.decodeObject(forKey: Key.avatarUrl.rawValue) as? String) ?? ""
    }
}
