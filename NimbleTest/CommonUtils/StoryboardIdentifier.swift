//
//  StoryboardIdentifier.swift
//  NimbleTest
//
//  Created by Dev on 9/13/20.
//  Copyright © 2020 Company. All rights reserved.
//

protocol StoryboardIdentifier: class {
    static var storyboardId: String { get }
}

extension StoryboardIdentifier {
    static var storyboardId: String {
        return String(describing: self)
    }
}
