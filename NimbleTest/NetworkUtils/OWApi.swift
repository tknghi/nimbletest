//
//  OWApi.swift
//  NimbleTest
//
//  Created by Dev Arya on 2017/06/15.
//  Copyright © 2020 Company. All rights reserved.
//

import Moya
import ObjectMapper

enum OWApi {
    // Account
    case login(email: String, password: String)
    case logout
    case resetPassword(email: String)
    case getAccount(userId: String)
    case refreshToken(refreshToken: String)
    
    //Survey
    case getSurvey(pageNumber: Int, pageSize: Int)
}

extension OWApi: TargetType {
    public var headers: [String : String]? {
        return [:]
    }
    
    public var baseURL: URL {
        return NBSettings.shared.serverResource.baseUrl
    }
    
    public var task: Task {
        switch self {
        case .login(let email, let password):
            return .requestParameters(parameters: [
                                        "grant_type": "password" as AnyObject,
                                        "email": email as AnyObject,
                                        "password": password as AnyObject,
                                        "client_id": NBSettings.shared.serverResource.clientId as AnyObject,
                                        "client_secret": NBSettings.shared.serverResource.clientSecret as AnyObject], encoding: JSONEncoding.default)
        case .refreshToken(let refreshToken):
            return .requestParameters(parameters: [
                                        "grant_type": "refresh_token" as AnyObject,
                                        "refresh_token": refreshToken as AnyObject,
                                        "client_id": NBSettings.shared.serverResource.clientId as AnyObject,
                                        "client_secret": NBSettings.shared.serverResource.clientSecret as AnyObject], encoding: JSONEncoding.default)
        case .resetPassword(let email):
            return .requestParameters(parameters: ["email": email as AnyObject], encoding: JSONEncoding.default)
        case .getSurvey(let pageNumber, let pageSize):
            return .requestParameters(parameters: [
                                        "page[number]": pageNumber as AnyObject,
                                        "page[size]": pageSize as AnyObject], encoding: URLEncoding.default)
        default:
            break
        }
        
        return .requestPlain
    }
    
    public var path: String {
        switch self {
        case .login:
            return "/api/v1/oauth/token"
        case .refreshToken:
            return "/api/v1/oauth/token"
        case .logout:
            return "/api/v1/oauth/revoke"
        case .resetPassword:
            return "/api/v1/passwords"
        case .getAccount:
            return "/api/v1/me"
        case .getSurvey:
            return "/api/v1/surveys"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .login,
             .refreshToken,
             .resetPassword,
             .logout:
            return .post
        case .getAccount,
             .getSurvey:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var validate: Bool {
        return true
    }
    
    public var accessToken: String {
        let authToken = NBKeychain.accessTokenString
        return authToken ?? ""
    }
}

public extension TargetType {
    func headers(header: String) -> [String: String] {
        return ["Authorization": header, "Content-Type":"application/json"]
    }
}
