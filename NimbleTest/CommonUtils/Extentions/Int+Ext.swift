//
//  Int+Ext.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/29/20.
//  Copyright © 2020 Company. All rights reserved.
//

import Foundation

extension Int {
    func degreesToRad() -> Double {
        return (Double(self) * .pi / 180)
    }
}
