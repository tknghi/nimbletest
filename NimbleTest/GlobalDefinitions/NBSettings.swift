//
//  Settings.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import Foundation

struct NBSettings {
    static var shared = NBSettings()
    
    struct ServerResource {
        var baseUrl = URL(string: "https://survey-api.nimblehq.co")!
        var clientId = ""
        var clientSecret = ""
    }
    
    private var productionServerResource: ServerResource = ServerResource(baseUrl: URL(string: "https://survey-api.nimblehq.co")!,
                                                                  clientId: "ofzl-2h5ympKa0WqqTzqlVJUiRsxmXQmt5tkgrlWnOE",
                                                                  clientSecret: "lMQb900L-mTeU-FVTCwyhjsfBwRCxwwbCitPob96cuU")
    
    //TODO: Configure client ID and cliend secrete key for stagging server if needed
    private var stagingServerResource: ServerResource = ServerResource(baseUrl: URL(string: "https://nimble-survey-web-staging.herokuapp.com")!,
                                                                       clientId: "",
                                                                       clientSecret: "")
    
    public private(set) var serverResource: ServerResource
    
    enum ServerEnvironment: String, CaseIterable {
        case production = "production"
        case staging = "staging"
    }
    
    var serverEnvironment: ServerEnvironment {
        set {
            switch newValue {
            case .staging:
                self.serverResource = stagingServerResource
            default:
                self.serverResource = productionServerResource
            }
            NBKeychain.serverEnvironment = newValue
        }
        get {
            return NBKeychain.serverEnvironment
        }
    }
    
    private init() {
        switch NBKeychain.serverEnvironment {
        case .staging:
            serverResource = stagingServerResource
        default:
            serverResource = productionServerResource
        }
    }
}
