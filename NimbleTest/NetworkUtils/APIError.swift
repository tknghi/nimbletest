//
//  APIError.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 8/17/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ObjectMapper

class APIError: NSObject, Mappable {
    var source: String = ""
    var detail: String = ""
    var code: String = ""
    
    // MARK: Object Mapper
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    // Mappable
    public func mapping(map: Map) {
        if let errs = map.JSON["errors"] as? [[String:String]], let err = errs.first {
            self.source = err["source"] ?? ""
            self.detail = err["detail"] ?? ""
            self.code   = err["code"] ?? ""
        }
    }
    
    class func defaultErrorWithData(_ data: Data) -> APIError? {
        if let apiError = Mapper<APIError>().map(JSONString: data.toString()), !apiError.code.isEmpty {
            return apiError
        }
        return nil
    }
}
