//
//  CommonViewController.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ReactiveSwift

class CommonViewController: UIViewController {
    
    var DEBUG: Bool = false
    var busyAlertController: UIAlertController?
    var disposables = CompositeDisposable()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bindViewModel()
    }
    
    func setupView() {
        self.view.backgroundColor = .lcBackground
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage.icArrowLeft, for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 0, height: 44.0)
        backButton.addTarget(self, action: #selector(backButtonTapped(sender:)), for: .touchUpInside)
        let backButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backButtonItem
    }
    
    
    func bindViewModel() {}
    
    @objc func backButtonTapped(sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension UIViewController {
    func showInfo(message: String) {
        let alert = UIAlertController(title: "Info", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(message: String, okHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: okHandler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String = "Alert", message: String, okHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: okHandler))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: cancelHandler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String, okTitle: String = "OK", okHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: okTitle, style: UIAlertAction.Style.default, handler: okHandler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String, okTitle: String, okHandler: ((UIAlertAction) -> Void)?, cancelTitle: String, cancelHandler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default, handler: cancelHandler))
        alert.addAction(UIAlertAction(title: okTitle, style: UIAlertAction.Style.default, handler: okHandler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showActionSheet(title: String, message: String, actions: [(String, UIAlertAction.Style)], completion: @escaping (_ index: Int) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for (index, (title, style)) in actions.enumerated() {
            let alertAction = UIAlertAction(title: title, style: style) { (_) in
                completion(index)
            }
            alert.addAction(alertAction)
        }
        self.present(alert, animated: true, completion: nil)
    }

    func promptForText(title: String, message: String, field: String, prefilledValue: String?, result: @escaping (Bool, String?)->Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addTextField { textField in
            textField.placeholderAttribute(text: field)
            if prefilledValue != nil {
                textField.text = prefilledValue!
            }
        }

        let okAction = UIAlertAction(title: "OK", style: .default) { [weak alertController] _ in
            if let alert = alertController {
                let textfield = alert.textFields?[0]
                result(true, textfield?.text ?? "")
            }
        }
        okAction.isEnabled = true
        alertController.addAction(okAction)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            result(false, nil)
        }
        alertController.addAction(cancelAction)

        DispatchQueue.main.async(
            execute: DispatchWorkItem(block: {
                self.present(alertController, animated: true, completion: nil) }))
    }
    
    /// Show a waiting dialog.
    /// Optionally pass cancelHandler to show a cancel button.
    func showBusyDialog(
        title: String,
        message: String,
        cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        
        let busyAlertController =
            UIAlertController(
                title: title,
                message: message,
                preferredStyle: UIAlertController.Style.alert)
        
        // add a spinning activity indicator
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        activityIndicator.color = .gray
        activityIndicator.startAnimating()
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: 22, height: 44)
        vc.view.addSubview(activityIndicator)
        busyAlertController.setValue(vc, forKey: "contentViewController")
        
        if cancelHandler != nil {
            busyAlertController.addAction(
                UIAlertAction(
                    title: "Cancel",
                    style: UIAlertAction.Style.default,
                    handler: cancelHandler!
                )
            )
        }
        self.present(busyAlertController, animated: false, completion: nil)
        return busyAlertController
    }
    
    /// show an alert message which will be self-dismissed after x seconds
    /// (default is 2 seconds)
    func showTimedAlert(title: String, message: String, duration: TimeInterval = TimeInterval(2)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - Special Pop/Push
extension CommonViewController {
    func viewControllerFromStoryboard<T: UIViewController>(vcKind: T.Type) -> T where T: StoryboardIdentifier {
        guard let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: vcKind.storyboardId) as? T else {
            fatalError("View controller with storyboard id \(vcKind.storyboardId) isn't existed!!!")
        }
        return vc
    }

    func popToViewController<T: UIViewController>(vcKind: T.Type? = nil, animated: Bool = true) {
        guard let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window else {
            return
        }
        if let vc = window?.rootViewController as? T {
            navigationController?.popToViewController(vc, animated: animated)
        } else if let vc = window?.rootViewController?.presentedViewController as? T {
            navigationController?.popToViewController(vc, animated: animated)
        } else if let vc = window?.rootViewController?.children.lazy.compactMap({ $0 as? T }).first {
            navigationController?.popToViewController(vc, animated: animated)
        }
    }
}

// MARK: - ActivityIndicator
class ActivityIndicator {
    
    private static let actIndView = UIActivityIndicatorView(style: .whiteLarge)
    private static var timer : Timer?
    
    static func start(_ operation: String, shouldShowTimeoutAlert: Bool = false, timeout: TimeInterval = 60.0) {
        stop()
        if actIndView.superview == nil, let keyWindow = UIApplication.shared.keyWindow {
            if Utils.isOnDarkMode {
                actIndView.color = .white
                actIndView.tintColor = .orange
            }
            else {
                actIndView.color = .gray
                actIndView.tintColor = .orange
            }
            
            keyWindow.isUserInteractionEnabled = false
            keyWindow.addSubview(actIndView)
            actIndView.center = keyWindow.center
            actIndView.startAnimating()
            // auto-stop in 10 seconds to prevent being stuck with a locked app
            timer =  Timer.scheduledTimer(
                withTimeInterval: timeout,
                repeats: false,
                block: { timer in
                    stop()
                    let timeoutMessage = "Time out waiting for operation \"" + operation + "\" to complete."
                    NSLog(timeoutMessage)
                    //Only display time out message when app is active
                    if shouldShowTimeoutAlert, UIApplication.shared.applicationState == .active, let vc = UIApplication.shared.keyWindow?.rootViewController {
                        vc.showAlert(message: timeoutMessage)
                    }
                }
            )
        }
    }
    
    static func stop() {
        if timer != nil {
            // Stop scheduled time-out action
            timer!.invalidate()
            timer = nil
        }
        if actIndView.superview != nil {
            actIndView.stopAnimating()
            actIndView.removeFromSuperview()
        }
        UIApplication.shared.keyWindow?.isUserInteractionEnabled = true
    }
}
