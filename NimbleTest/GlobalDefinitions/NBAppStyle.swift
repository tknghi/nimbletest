//
//  LCLCAppStyle.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

func localize(_ text: String) -> String {
    return NSLocalizedString(text, comment: "")
}

class NBAppStyle {
    enum DIMENSION_CONSTANT: CGFloat {
        case BOTTOM_PANEL_HEIGHT = 80
    }
    
    enum OFFSETS: CGFloat {
        case FIVE_PHONE = 5
        case FIVE_PAD = 9
    }
    
    //Text lenght
    struct TextLength {
        static let userId: Int = 140
        static let password: Int = 140
    }
}
