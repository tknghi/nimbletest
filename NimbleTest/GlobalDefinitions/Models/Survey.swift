//
//  Survey.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import ObjectMapper

class Survey: NSObject, Mappable {
    enum Key: String {
        case id                 = "id"
        case type               = "type"
        case title              = "attributes.title"
        case description        = "attributes.description"
        case coverImageUrl      = "attributes.cover_image_url"
    }

    var id: String = ""
    var type: String = ""
    var title: String = ""
    var surveyDescription: String = ""
    var coverImageUrl: String = ""
    
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        self.id                 <- map[Key.id.rawValue]
        self.type               <- map[Key.type.rawValue]
        self.title              <- map[Key.title.rawValue]
        self.surveyDescription  <- map[Key.description.rawValue]
        self.coverImageUrl      <- map[Key.coverImageUrl.rawValue]
    }
}
