//
//  FileManager+Ext.swift
//  Company
//
//  Created by Developer on 6/15/18.
//  Copyright © 2018 Company. All rights reserved.
//

import UIKit

extension FileManager {
    class func pathInDocumentsDir(file: String) -> String {
        return "\(FileManager.documentsDir())/\(file)"
    }
    
    class func pathInCachesDir(file: String) -> String {
        return "\(FileManager.cachesDir())/\(file)"
    }
    
    class func pathInTempDir(file: String) -> String {
        return "\(FileManager.tempDir())/\(file)"
    }
    
    class func sizeOfFile(_ filePath: String) -> UInt64 {
        var fileSize : UInt64 = 0
        
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: filePath)
            fileSize = attr[FileAttributeKey.size] as! UInt64
        } catch {
            print("Error: \(error)")
        }
        
        return fileSize
    }
    
    class func documentsDir() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths.first!
    }
    
    class func cachesDir() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths.first!
    }
    
    class func tempDir() -> String {
        let paths = NSTemporaryDirectory()
        return paths
    }
}
