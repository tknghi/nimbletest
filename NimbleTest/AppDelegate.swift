//
//  AppDelegate.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    static let shared = UIApplication.shared.delegate as! AppDelegate
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        configureServerEnvironment()
        handleExistedUserFlow()
        
        return true
    }
}

//MARK: - Utility
extension AppDelegate {
    func configureServerEnvironment() {
        #if !DEBUG
        
        NBSettings.shared.serverEnvironment = .production
        
        #else
        
        if let fsEnv        = UserDefaults.standard.string(forKey: "FS_ENDPOINT"), fsEnv.count > 0,
           let serverType   = NBSettings.ServerEnvironment.init(rawValue: fsEnv) {
            print("fsEnv = \(fsEnv)")
            switch serverType {
            case .staging:
                NBSettings.shared.serverEnvironment = .staging
            default:
                NBSettings.shared.serverEnvironment = .production
            }
        }
        else {
            NBSettings.shared.serverEnvironment = .production
        }
        
        #endif
    }
    
    func handleExistedUserFlow() {
        if let _ = NBKeychain.user {
            guard let mainNavCtrl = window?.rootViewController as? UINavigationController,
                let activationCodeVC = GlobalVariables.surveyStoryboard.instantiateInitialViewController() as? NBSurveyViewController else {
                return
            }
            
            mainNavCtrl.pushViewController(activationCodeVC, animated: false)
        }
    }
    
    func endSession() {
        //Clear local data here
        NBKeychain.removeAllKeychainValues()
        
        guard let mainNavCtrl = window?.rootViewController as? UINavigationController else {
            NSLog("FATAL: No root view controller")
            return
        }
        
        mainNavCtrl.popToRootViewController(animated: true)
    }
}

