//
//  NBAccountSigninViewController.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/29/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit
import ReactiveSwift

class NBAccountSigninViewController: CommonViewController {

    @IBOutlet weak var signInView: NBSigninView!
    @IBOutlet weak var signInViewBottomMargin: NSLayoutConstraint!
    
    let signInViewModel = NBAccountSigninViewModel()
    
    deinit {
        disposables.dispose()
    }
    
    // MARK: - Life circle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Notification for keyboard show/hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        // prevent returning to this screen with some field still in focus
        signInView.endEditing(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // clear all fields before leaving for security
        signInView.reset()
    }
    
    // MARK: - User Events
    override func bindViewModel() {
        signInView.didEnterIdAndPassword = { [weak self] (id, password) in
            guard let strongSelf = self else { return }
            ActivityIndicator.start(String.emptyString, shouldShowTimeoutAlert: false, timeout: 10)
            strongSelf.signInViewModel.signIn.apply((id, password, CloudAPIManager.provider)).start()
        }
        
        disposables += self.signInViewModel.signIn.values.observeValues { [unowned self] success in
            if success {
                self.signInViewModel.loadAccount.apply(CloudAPIManager.provider).start()
            } else {
                ActivityIndicator.stop()
                
                // This flow should not happen. We returned error when parsing failed.
                self.showAlert(message: localize("Could not sign in at the moment. Please try again later."))
            }
        }
        
        disposables += self.signInViewModel.signIn.errors.observeValues { [unowned self] value in
            ActivityIndicator.stop()
            var error = localize("Error during sign in. Please check your internet connection")
            switch value {
            case .loginFailed:
                error = localize("Invalid username or password")
            case .userBlocked:
                error = localize("Your account is blocked. Please try again after 1 day")
            case .userSuspended:
                error = localize("Your account has been suspended. Contact administrator for more information")
            case .emailNotVerified:
                error = String(format: localize("Your account is not verified. Please check the confirmation email at %@"), self.signInView.idTextField.text)
            default:
                break
            }
            
            self.showAlert(message: error)
        }
        
        disposables += self.signInViewModel.loadAccount.values.observeValues { [unowned self] user in
            ActivityIndicator.stop()
            self.handleValidatingUserInfo(user: user)
        }
        
        disposables += self.signInViewModel.loadAccount.errors.observeValues { [unowned self] error in
            ActivityIndicator.stop()
            self.showAlert(message: localize("Failed to load account information."))
        }
    }
    
    @IBAction func backgroundTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // Workaroud to avoid calling updating too much: https://stackoverflow.com/questions/32860151/uikeyboardwillshownotification-calling-twice-ios-9
        
        var animationDuration = 0.0
        var animationCurve: UIView.AnimationOptions = .beginFromCurrentState
        let keyboardHeight = Utils.keyboardHeightWithNotification(notification, onView: view, animationDuration: &animationDuration, animationCurve: &animationCurve)
        
        if keyboardHeight > signInViewBottomMargin.constant {
            updateBottomLayoutConstraintWithNotification(height: keyboardHeight, animationDuration: animationDuration, animationOption: animationCurve, padding: 16)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        var animationDuration = 0.0
        var animationCurve: UIView.AnimationOptions = .beginFromCurrentState
        let keyboardHeight = Utils.keyboardHeightWithNotification(notification, onView: view, animationDuration: &animationDuration, animationCurve: &animationCurve)
        
        if signInViewBottomMargin.constant > 50 {
            updateBottomLayoutConstraintWithNotification(height: keyboardHeight, animationDuration: animationDuration, animationOption: animationCurve, padding: 50)
        }
    }

    // MARK: - Utilities
    func updateBottomLayoutConstraintWithNotification(height: CGFloat, animationDuration: Double, animationOption: UIView.AnimationOptions , padding: CGFloat = 0.0) {
        UIView.animate(withDuration: animationDuration, delay: 0.0, options: [UIView.AnimationOptions.beginFromCurrentState, animationOption], animations: {
            self.signInViewBottomMargin.constant = height + padding
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func handleValidatingUserInfo(user: User) {
        if !user.id.isEmpty {
            performSegue(withIdentifier: GlobalVariables.SegueIdentifier.surveyList.rawValue, sender: self)
        } else {
            showAlert(title: localize("Warning"), message: localize("Could not load user information with this session.")) { [unowned self] (_) in
                self.signInViewModel.signOut.apply(CloudAPIManager.provider).start()
                // Don't mind about the invalid user -> Logout anyway
                performSegue(withIdentifier: GlobalVariables.SegueIdentifier.surveyList.rawValue, sender: self)
            }
        }
    }
}
