//
//  UICollectionView+Reusable.swift
//  NimbleTest
//
//  Created by Dev on 9/16/20.
//  Copyright © 2020 Company. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    final func register<T: UICollectionViewCell>(cellType: T.Type) where T: Reusable {
        register(cellType.self, forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func registerNib<T: UICollectionViewCell>(cellType: T.Type) where T: Reusable {
        register(UINib(nibName: cellType.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func registeReusableView<T: UICollectionReusableView>(cellType: T.Type, kind: String) where T: Reusable {
        register(cellType, forSupplementaryViewOfKind: kind, withReuseIdentifier: cellType.reuseIdentifier)
    }

    final func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T where T: Reusable {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            fatalError(
                "Failed to dequeue a cell with identifier \(cellType.reuseIdentifier) matching type \(cellType.self). "
                    + "Check that the reuseIdentifier is set properly in your XIB/Storyboard "
                    + "and that you registered the cell beforehand"
            )
        }
        return cell
    }
    
    final func dequeueReusableSupplementaryView<T: UICollectionReusableView>(for indexPath: IndexPath, cellType: T.Type = T.self, kind: String) -> T where T: Reusable {
        guard let cell = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            fatalError(
                "Failed to dequeue a cell with identifier \(cellType.reuseIdentifier) matching type \(cellType.self). "
                    + "Check that the reuseIdentifier is set properly in your XIB/Storyboard "
                    + "and that you registered the cell beforehand"
            )
        }
        return cell
    }
}
