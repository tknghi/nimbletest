//
//  NBSurveyCollectionViewCell.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import Kingfisher

class NBSurveyCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var surveyTitleLabel: UILabel!
    @IBOutlet private weak var surveyDescriptionLabel: UILabel!
    @IBOutlet private weak var takeSurveyButton: UIButton!
    @IBOutlet private weak var corverImageView: UIImageView!

    public private(set) var survey: Survey?
    weak var surveysViewModel: NBSurveyListViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        surveyTitleLabel.setup(text: "", size: .fontSize20, weight: .semibold)
        surveyTitleLabel.numberOfLines = 0
            
        surveyDescriptionLabel.setup(text: "", size: .fontSize14, weight: .semibold)
        surveyDescriptionLabel.numberOfLines = 0
        
        takeSurveyButton.layer.cornerRadius = 24.0
        takeSurveyButton.clipsToBounds = true
    }

    func displaySurvey(survey: Survey) {
        surveyTitleLabel.text = ""
        surveyDescriptionLabel.text = ""
        self.survey = survey
        
        surveyTitleLabel.text = survey.title
        surveyDescriptionLabel.text = survey.surveyDescription
        
        guard let url = URL(string: survey.coverImageUrl) else { return }
        let resource = ImageResource(downloadURL: url)
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { [weak self] result in
            switch result {
            case .success(let value):
                self?.corverImageView.image = value.image
            case .failure(_):
                self?.corverImageView.image = UIImage(named: "bg-signin-414")
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func takeSurvey(_ sender: Any) {
        surveysViewModel?.currentSurvey.value = self.survey
    }
}
