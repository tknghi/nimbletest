//
//  NBSurveyListViewModel.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import ReactiveSwift

class NBSurveyListViewModel: NSObject {
    static let pageSize: Int = 3
        
    var surveys: MutableProperty<[Survey]> = MutableProperty<[Survey]>([])
    var currentSurvey: MutableProperty<Survey?> = MutableProperty<Survey?>(nil)
    var displayingSurvey: MutableProperty<Survey?> = MutableProperty<Survey?>(nil)
    var pageNumder: Int = 1
    var numberOfPage: Int = 1
    var maxNumberOfSurveys: Int = 0
    
    let loadSurveys: Action<(Int, Int, Networking), ([Survey], SurveyMetaData), SurveyServiceError>
    
    init(debug: Bool = false) {
        let surveyService: SurveyService
    
        if (!debug) {
            surveyService = SurveyServiceImpl()
        } else {
            surveyService = SurveyServiceDebug()
        }
        
        loadSurveys = Action { input in
            return surveyService.getSurvey(pageNumber: input.0, pageSize: input.1, provider: input.2)
        }
    }
    
    func loadServeys() {
        surveys.value.removeAll()
        pageNumder = 1
        maxNumberOfSurveys = 0
        
        loadSurveys.apply((pageNumder, NBSurveyListViewModel.pageSize, CloudAPIManager.provider)).startWithResult { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success((let surveys, let metaData)):
                strongSelf.handleNewLoadedSurveys(newSurveys: surveys, metaData: metaData)
                break
            case .failure(_):
                break
            }
        }
    }
    
    func loadMoreServeys() -> Bool {
        if pageNumder >= numberOfPage {
            return false
        }
        loadSurveys.apply((pageNumder + 1, NBSurveyListViewModel.pageSize, CloudAPIManager.provider)).startWithResult { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case .success((let surveys, let metaData)):
                strongSelf.handleNewLoadedSurveys(newSurveys: surveys, metaData: metaData)
                break
            case .failure(_):
                break
            }
        }
        
        return true
    }
}

fileprivate extension NBSurveyListViewModel {
    func handleNewLoadedSurveys(newSurveys: [Survey], metaData: SurveyMetaData) {
        self.pageNumder = metaData.page
        self.maxNumberOfSurveys = metaData.records
        self.numberOfPage = metaData.pages
        self.appendSurveys(newSurveys: newSurveys)
    }
    func appendSurveys(newSurveys: [Survey]) {
        
        //TODO: Check for redundancy. Just use this for demo only
        
        var _surveys = self.surveys.value
        _surveys.append(contentsOf: newSurveys)
        self.surveys.value = _surveys
    }
}
