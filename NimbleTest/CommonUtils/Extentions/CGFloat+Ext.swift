//
//  CGFloat+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension CGFloat {
    static let fontSize24: CGFloat          = 24.0
    static let fontSize20: CGFloat          = 20.0
    static let fontSize18: CGFloat          = 18.0
    static let fontSize17: CGFloat          = 17.0
    static let fontSize16: CGFloat          = 16.0
    static let fontSize14: CGFloat          = 14.0
    static let textFieldFontSize: CGFloat   = 14.0
    static let textViewFontSize: CGFloat    = 14.0
    static let deviceRadio: CGFloat         = Utils.isIPad ? 1.0 : 1.0

    static func applyDesignRadio(with constraint: CGFloat, withScreenWidth: Bool) -> CGFloat {
        let newConstraint = withScreenWidth ? screenWidth * constraint / 375.0 : screenHeight * constraint / 812.0
        if newConstraint < constraint {
            return newConstraint
        } else {
            return constraint
        }
    }
}
