//
//  String+Ext.swift
//  Company
//
//  Created by Nghi Tran Inc on 7/10/16.
//  Copyright © 2017 Company. All rights reserved.
//

import Foundation
import UIKit

extension String {
    static let emptyString = ""

    var isNumber: Bool {
        let characters = CharacterSet.decimalDigits.inverted
        return !self.isEmpty && rangeOfCharacter(from: characters) == nil
    }

    // returns a case-insensitive, diacritic-insensitive string for sorting
    var sortKey: String {
        get {
            return self.folding(options: [.diacriticInsensitive, .caseInsensitive], locale: .current)
        }
    }

    func substringToIndex(index: Int) -> String {
        let index: String.Index = self.index(self.startIndex, offsetBy: index)
        return String(self[..<index])
    }
    
    func substringFromIndex(index: Int) -> String {
        let index: String.Index = self.index(self.startIndex, offsetBy: index)
        return String(self[index...])
    }
    
    func substringsMatched(pattern: String) -> [String] {
        var subStrs: [String] = []
        
        do {
            let regex = try NSRegularExpression(pattern: pattern)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            subStrs = results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("No substring matchs regex: \(error.localizedDescription)")
        }
        
        return subStrs
    }
    
    func trimSpace() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}

extension String {
    
    func capitalizingFirstLetter() -> String {
        guard self.count > 0 else { return self }

        let tmpString = self.lowercased()
        let first = String(tmpString.prefix(1)).capitalized
        let other = String(tmpString.dropFirst())
        return first + other
    }
    
    func attributeString(_ atrString: String, mainStr: String, color: UIColor, font: UIFont){
        
        let mutableAtrStr = NSMutableAttributedString(string: mainStr)
        let attribute = [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font]
        let attributeString = NSAttributedString(string: atrString, attributes: attribute)
        mutableAtrStr.append(attributeString)
    }
    
    /**
     format some string in normal string.
     */
    func formatStringInString(_ string: String,
                              font: UIFont = UIFont.systemFont(ofSize: 14),
                              color: UIColor = UIColor.black,
                              boldStrings: [String],
                              boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                              boldColor: UIColor = UIColor.blue
        ) -> NSAttributedString {
        
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        for bold in boldStrings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
    
    func formatParagraph(_ string: String, alignText: NSTextAlignment = NSTextAlignment.left, spacing: CGFloat = 7) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.alignment = alignText
        paragraphStyle.maximumLineHeight = 40
        
        let attributed = [NSAttributedString.Key.paragraphStyle:paragraphStyle]
        return NSAttributedString(string: string, attributes:attributed)
    }
    
    func strikethroughText() -> NSMutableAttributedString {
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: NSUnderlineStyle.single.rawValue,
                                     range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    func estimateFrameForText(withFont font: UIFont, estimateSize: CGSize) -> CGRect {
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: self).boundingRect(with: estimateSize, options: options, attributes: [NSAttributedString.Key.font: font], context: nil)
    }
    
    func highlight(text: String, color: UIColor = .black, font: UIFont) -> NSMutableAttributedString {
        let nsstring = NSString.init(string: self)
        let range = nsstring.range(of: text)
        let attr = NSMutableAttributedString.init(string: self)
        
        guard range.location == NSNotFound else {
            return attr
        }
        
        attr.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color], range: range)
        return attr
    }
}

extension String {
    func formatThousandSeparator(_ separatorCharacter: String = " ") -> String {
        
        guard let numberFromString = Int32(self) else { return self }
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = separatorCharacter
        let formattedString = formatter.string(from: NSNumber(value: numberFromString as Int32))
        return formattedString != nil ? formattedString! : self
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func splitString(_ separator: String) -> [String] {
        return components(separatedBy: separator)
    }
    
    static func localize(key : String) -> String{
        let currentLanguage = Context.getCurrentLanguageCode()
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            let languageBundle = Bundle(path: path)
            let result = languageBundle?.localizedString(forKey: key, value: "", table: nil)
            return result ?? key
        }
        return key
    }
    
    func replaceRegexMatches(pattern: String, replaceWith: String = "") -> String {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, self.count)
            let newString = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
            return newString
        } catch {
            return ""
        }
    }
}

public extension String {
    
    // https://gist.github.com/stevenschobert/540dd33e828461916c11
    func camelize() -> String {
        let source = clean(" ", allOf: "-", "_")
        if source.contains(" ") {

            let first = source[source.startIndex]
            let cammel = NSString(format: "%@", (source as NSString).capitalized.replacingOccurrences(of: " ", with: "", options: [], range: nil)) as String
            let rest = String(cammel.dropFirst())
            return "\(first)\(rest)"
        } else {
            let first = (source as NSString).lowercased[source.startIndex]
            let rest = String(source.dropFirst())
            return "\(first)\(rest)"
        }
    }
    
    func contains(_ substring: String) -> Bool {
        return range(of: substring) != nil
    }
    
    func chompLeft(_ prefix: String) -> String {
        if let prefixRange = range(of: prefix) {
            if prefixRange.upperBound >= endIndex {
                return String(self[startIndex..<prefixRange.lowerBound])
            } else {
                return String(self[prefixRange.upperBound..<endIndex])
            }
        }
        return self
    }
    
    func chompRight(_ suffix: String) -> String {
        if let suffixRange = range(of: suffix, options: .backwards) {
            if suffixRange.upperBound >= endIndex {
                return String(self[startIndex..<suffixRange.lowerBound])
            } else {
                return String(self[suffixRange.upperBound..<endIndex])
            }
        }
        return self
    }
    
    func collapseWhitespace() -> String {
        let components = self.components(separatedBy: CharacterSet.whitespacesAndNewlines).filter { !$0.isEmpty }
        return components.joined(separator: " ")
    }
    
    func clean(_ with: String, allOf: String...) -> String {
        var string = self
        for target in allOf {
            string = string.replacingOccurrences(of: target, with: with)
        }
        return string
    }
    
    func count(_ substring: String) -> Int {
        return components(separatedBy: substring).count - 1
    }
    
    func endsWith(_ suffix: String) -> Bool {
        return hasSuffix(suffix)
    }
    
    func ensureLeft(_ prefix: String) -> String {
        return startsWith(prefix) ? self : "\(prefix)\(self)"
    }
    
    func ensureRight(_ suffix: String) -> String {
        return endsWith(suffix) ? self : "\(self)\(suffix)"
    }
    
    func indexOf(_ substring: String) -> Int? {
        
        guard let range = range(of: substring) else { return nil }
        return self.distance(from: startIndex, to: range.lowerBound)
    }
    
    func lastIndexOf(_ target: String) -> Int? {
        guard let range = range(of: target, options: .backwards) else { return nil }
        return self.distance(from: startIndex, to: range.lowerBound)
    }
    
    func isAlpha() -> Bool {
        for chr in self {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    func isAlphaNumeric() -> Bool {
        let alphaNumeric = CharacterSet.alphanumerics
        return components(separatedBy: alphaNumeric).joined(separator: "").count == 0
    }
    
    func isNumeric() -> Bool {
        return NumberFormatter().number(from: self) != nil
    }
    
    func startsWith(_ prefix: String) -> Bool {
        return hasPrefix(prefix)
    }
    
    func isIPV4() -> Bool {
        let regex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: self)
    }
    
    static func random(_ length: Int = 5) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        
        return randomString
    }
    
    func urlStringWithoutParams() -> String {
        if let urlComponents = NSURLComponents(string: self) {
            urlComponents.query = nil; // Strip out query parameters.
            if let urlWithoutParams = urlComponents.string {
                return urlWithoutParams
            }
        }
        return self
    }
}

extension String {
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}

//More func
extension String {
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    subscript (bounds: PartialRangeUpTo<Int>) -> String {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[..<end])
    }
}

extension String{
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
}

// MARK: Add space before capital letter in String

extension String {
    func titlecased() -> String {
        return self.replacingOccurrences(of: "([A-Z])", with: " $1", options: .regularExpression, range: self.range(of: self))
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .capitalized
    }
}

// MARK: Convert to dictionary
extension String {
    func toDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                return nil
            }
        }
        return nil
    }
}

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var containsOnlyDecimalDigits: Bool {
        get {
            return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
        }
    }
    
    var containsOnlyHexadecimalDigits: Bool {
        get {
            return CharacterSet.hexadecimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
        }
    }
}

extension CharacterSet {
    static var hexadecimalDigits: CharacterSet {
        get {
            return CharacterSet(charactersIn: "0123456789AaBbCcDdEeFf")
        }
    }
}




