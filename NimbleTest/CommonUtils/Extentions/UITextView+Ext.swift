//
//  UITextView+Ext.swift
//  NimbleTest
//
//  Created by Dev on 8/25/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

extension UITextView {
    func errorTextViewStyle() {
        self.font = UIFont.systemFont(ofSize: .textViewFontSize)
        self.textColor = .lcRed
    }

    func infoTextViewStyle() {
        self.font = UIFont.systemFont(ofSize: .textViewFontSize)
        self.textColor = .lcGrey
    }
}
