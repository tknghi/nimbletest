//
//  SurveyMetaData.swift
//  NimbleTest
//
//  Created by Nghi Tran on 11/30/20.
//

import UIKit
import ObjectMapper

class SurveyMetaData: NSObject, Mappable {
    enum Key: String {
        case page       = "page"
        case pages      = "pages"
        case pageSize   = "page_size"
        case records    = "records"
    }

    var page: Int = 1
    var pages: Int = 0
    var pageSize: Int = 0
    var records: Int = 0
    
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        self.page     <- map[Key.page.rawValue]
        self.pages    <- map[Key.pages.rawValue]
        self.pageSize <- map[Key.pageSize.rawValue]
        self.records  <- map[Key.records.rawValue]
    }
}
