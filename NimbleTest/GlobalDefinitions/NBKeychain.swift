//
//  LCKeychain.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//

import SwiftKeychainWrapper

public enum KEYCHAIN_DEFINITION: String {
    case NB_USER = "NBUser"
    case NB_SERVER_ENVIRONMENT = "NBServerEnvironment"
    case NB_ACCESS_TOKEN = "NBAccessToken"
    case NB_ACCESS_TOKEN_EXPIRATION = "NBAccessTokenExpiration"
    case NB_REFRESH_TOKEN = "NBRefreshToken"
    case NB_TOKEN_TYPE = "NBTokenType"
}

public enum KEYCHAIN_SESSION_DEFINITION: String {
    case DEVICE_SETS = "LCDeviceSets"
    case ACTIVATION_CODE = "LCActivationCode"
}

class NBKeychain {

    static func debugPrint() {
        
    }
    
    class var serverEnvironment: NBSettings.ServerEnvironment {
        get {
            return NBSettings.ServerEnvironment(rawValue: KeychainWrapper.standard.string(forKey: KEYCHAIN_DEFINITION.NB_SERVER_ENVIRONMENT.rawValue) ?? "") ?? NBSettings.ServerEnvironment.staging
        }
        set(newValue) {
            KeychainWrapper.standard.set(newValue.rawValue, forKey: KEYCHAIN_DEFINITION.NB_SERVER_ENVIRONMENT.rawValue)
        }
    }
    
    /// Access token (including both its string and expiration time)
    @available(*, deprecated, message: "Don't need this with the solution if freshtoken")
    class var accessToken: (string: String, expiration: Date)? {
        get {
            if let string = Self.accessTokenString,
               let expiration = Self.accessTokenExpiration {
                return (string: string, expiration: expiration)
            }
            return nil
        }
        set (newValue) {
            accessTokenString = newValue?.string
            accessTokenExpiration = newValue?.expiration
        }
    }
    
    /// Access token
    class var accessTokenString: String? {
        get {
            return KeychainWrapper.standard.string(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN.rawValue)
        }
        set (newValue) {
            if let value = newValue {
                KeychainWrapper.standard.set(value, forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN.rawValue)
            }
            else {
                KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN.rawValue)
            }
        }
    }
    
    /// Refresh token used to get a new access token
    class var refreshTokenString: String? {
        get {
            return KeychainWrapper.standard.string(forKey: KEYCHAIN_DEFINITION.NB_REFRESH_TOKEN.rawValue)
        }
        set (newValue) {
            if let value = newValue {
                KeychainWrapper.standard.set(value, forKey: KEYCHAIN_DEFINITION.NB_REFRESH_TOKEN.rawValue)
            }
            else {
                KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_REFRESH_TOKEN.rawValue)
            }
        }
    }
    
    /// Refresh token used to get a new access token
    class var tokenTypeString: String? {
        get {
            return KeychainWrapper.standard.string(forKey: KEYCHAIN_DEFINITION.NB_TOKEN_TYPE.rawValue)
        }
        set (newValue) {
            if let value = newValue {
                KeychainWrapper.standard.set(value, forKey: KEYCHAIN_DEFINITION.NB_TOKEN_TYPE.rawValue)
            }
            else {
                KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_TOKEN_TYPE.rawValue)
            }
        }
    }
    
    /// Expiration time of the access token in the key chain
    private class var accessTokenExpiration: Date? {
        get {
            if let epochTime = KeychainWrapper.standard.double(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN_EXPIRATION.rawValue) {
                return Date(timeIntervalSince1970: epochTime)
            }
            return nil
        }
        set(newValue) {
            if let epochTime = newValue?.timeIntervalSince1970 {
                KeychainWrapper.standard.set(epochTime, forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN_EXPIRATION.rawValue)
            }
            else {
                KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN_EXPIRATION.rawValue)
            }
        }
    }
    
    /// Whether the currently access token in the keychain has already expired (true if access token expiration not set)
    class var accessTokenExpired: Bool {
        get {
            if let expiration = Self.accessTokenExpiration {
                return Date() >= expiration
            }
            return true
        }
    }
    
    class var user: User? {
        get {
            if let decodedData  = KeychainWrapper.standard.data(forKey: KEYCHAIN_DEFINITION.NB_USER.rawValue) {
                return NSKeyedUnarchiver.unarchiveObject(with: decodedData) as? User
            }
            else {
                return nil
            }
        }
        set(newValue) {
            if let _user = newValue {
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: _user)
                KeychainWrapper.standard.set(encodedData, forKey: KEYCHAIN_DEFINITION.NB_USER.rawValue)
            }
            else {
                KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_USER.rawValue)
            }
        }
    }
    
    // This is especially useful when the user log-off.
    static func removeAllKeychainValues() {
        KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN.rawValue)
        KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_REFRESH_TOKEN.rawValue)
        KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_ACCESS_TOKEN_EXPIRATION.rawValue)
        KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_TOKEN_TYPE.rawValue)
        KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_DEFINITION.NB_USER.rawValue)
    }
    
    static func removeKey(keychain: KEYCHAIN_DEFINITION) {
        KeychainWrapper.standard.removeObject(forKey: keychain.rawValue)
    }
}

//MARK: - Utility definations
class SurveyTuple: NSObject, NSCoding {
    let surveys: [Survey]
    
    override init() {
        self.surveys = [Survey]()
    }
    
    init(surveys: [Survey]) {
        self.surveys = surveys
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.surveys, forKey: "surveys")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let surveys = aDecoder.decodeObject(forKey: "surveys") as! [Survey]
        self.init(surveys: surveys)
    }
}
