//
//  UIView+Ext.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/27/20.
//  Copyright © 2020 Origin Wireless. All rights reserved.
//

import UIKit

extension UIView {
    func removeAllSubViews() {
        for subview in self.subviews {
            subview.removeFromSuperview();
        }
    }
}
