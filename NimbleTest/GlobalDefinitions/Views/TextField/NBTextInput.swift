//
//  NBTextField.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/30/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

enum NBTextFieldState: Int {
    case normal
    case focused
    case error
}

protocol NBTextInputDelegate: AnyObject {
    func textFieldShouldBeginEditing(_ textField: NBTextInput) -> Bool
    func textFieldDidBeginEditing(_ textField: NBTextInput)
    func textFieldDidEndEditing(_ textField: NBTextInput)
    func textField(_ textField: NBTextInput, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    func textFieldShouldClear(_ textField: NBTextInput) -> Bool
    func textFieldShouldReturn(_ textField: NBTextInput) -> Bool
}

extension NBTextInputDelegate {
    func textFieldShouldBeginEditing(_ textField: NBTextInput) -> Bool { return true }
    func textFieldDidBeginEditing(_ textField: NBTextInput) {}
    func textFieldDidEndEditing(_ textField: NBTextInput) {}
    func textField(_ textField: NBTextInput, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { return true }
    func textFieldShouldClear(_ textField: NBTextInput) -> Bool { return true }
    func textFieldShouldReturn(_ textField: NBTextInput) -> Bool { return true }
}

class NBTextInput: BaseView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: NBTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownIV: UIImageView!
    @IBOutlet weak var titleLoadingIndicator: UIActivityIndicatorView!

    weak var delegate: NBTextInputDelegate?
    var padding: CGFloat = 5
    var lcState: NBTextFieldState = .normal {
        didSet {
            switch lcState {
            case .normal:
                self.textField.layer.borderColor = UIColor.lcBlack20.cgColor
                self.errorLabel.isHidden = true
                self.errorLabelHeight.constant = 0.0
            case .focused:
                self.textField.layer.borderColor = UIColor.lcBlack.cgColor
                self.errorLabel.isHidden = true
                self.errorLabelHeight.constant = 0.0
            case .error:
                self.textField.layer.borderColor = UIColor.lcRed.cgColor
                self.errorLabel.isHidden = false
                
                var errLabelHeight: CGFloat = 30.0
                if let errorContentHeight = errorLabel.text?.heightWithConstrainedWidth(self.bounds.width, font: UIFont.systemFont(ofSize: 14.0)),
                    errorContentHeight > errLabelHeight {
                    errLabelHeight = errorContentHeight
                }
                self.errorLabelHeight.constant = errLabelHeight
            }
            self.layoutSubviews()
            self.layoutIfNeeded()
        }
    }
    
    func reset() {
        text = ""
        lcState = .normal
        endEditing(true)
    }
    
    var isPasswordField: Bool {
        get {
            return textField.isPasswordField
        }
        set {
            textField.isPasswordField = newValue
        }
    }
    
    var text: String {
        get {
            return self.textField.text ?? ""
        }
        set {
            self.textField.text = newValue
        }
    }
    
    var title: String {
        get {
            return self.titleLabel.text ?? ""
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    var placeholder: String {
        get {
            return self.textField.placeholder ?? ""
        }
        set {
            self.textField.placeholder = newValue
        }
    }
    
    var errorText: String {
        get {
            return self.errorLabel.text ?? ""
        }
        set {
            self.errorLabel.text = newValue
            if !newValue.isEmpty {
                self.lcState = .error
            }
            else {
                self.lcState = .normal
            }
        }
    }

    var isShowTitleLoadingIndicator: Bool = false {
        didSet {
            titleLoadingIndicator.isHidden = !isShowTitleLoadingIndicator
            isShowTitleLoadingIndicator ? titleLoadingIndicator.startAnimating() : titleLoadingIndicator.stopAnimating()
        }
    }

    var isShowDropDown: Bool = false {
        didSet {
            dropDownIV.isHidden = !isShowDropDown
        }
    }

    var isDropDown: Bool = true {
        didSet {
            dropDownIV.image = isDropDown ? .icDropDown : .icDropUp
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setupView() {
        super.setupView()
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        titleLabel.text = nil
        titleLabel.textColor = .lcBlack70

        textField.text = nil
        textField.textColor = .lcBlack
        
        errorLabel.text = nil
        errorLabel.textColor = .lcRed

        titleLoadingIndicator.hidesWhenStopped = true
    }
}

extension NBTextInput: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldBeginEditing(self) ?? true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        lcState = .focused
        delegate?.textFieldDidBeginEditing(self)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        lcState = .normal
        delegate?.textFieldDidEndEditing(self)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return delegate?.textField(self, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldClear(self) ?? true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldReturn(self) ?? true
    }
}
