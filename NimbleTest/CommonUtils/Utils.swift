//
//  Utils.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 Company. All rights reserved.
//


import UIKit
import SystemConfiguration.CaptiveNetwork
import NetworkExtension

// MARK: - Global variables

var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

var screenSize: CGRect {
    return UIScreen.main.bounds
}

var mainScreen: UIScreen {
    return UIScreen.main
}

var statusBarStyle = UIStatusBarStyle.lightContent {
    didSet {
        //Setter for 'statusBarStyle' was deprecated in iOS 9.0: Use -[UIViewController preferredStatusBarStyle]
//        UIApplication.shared.statusBarStyle = statusBarStyle
    }
}

// MARK: - Global functions

func timeFormatted(_ totalSeconds: Int) -> String {
    let minutes: Int = (totalSeconds / 60) % 60
    let hours: Int = totalSeconds / 3600
    return (hours > 0 ? "\(hours)h " : "") + (minutes > 0 ? "\(minutes)m" : "0m")
}

func hideKeyboard() {
    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
}


var statusBarHidden = false {
    didSet {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.isHidden = statusBarHidden
    }
}


var statusBarBackgroundColor: UIColor = .clear {
    didSet {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = statusBarBackgroundColor
    }
}

func makeCall(to number: String) {
    guard let phoneUrl = URL(string: "tel://\(number)") else { return }
    guard UIApplication.shared.canOpenURL(phoneUrl) else { return }
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(phoneUrl)
    } else {
        UIApplication.shared.openURL(phoneUrl)
    }
}

var buildDate: Date
{
    if let infoPath = Bundle.main.path(forResource: "Info.plist", ofType: nil),
        let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
        let infoDate = infoAttr[FileAttributeKey.creationDate] as? Date
    {
        return infoDate
    }
    return Date()
}

// MARK: Concurrency utils
var taskDelay = NSMutableArray()

func performSelectorWithClause(afterDelay delay: TimeInterval, clause: @escaping ()->Void)  {
    let additionalTime: DispatchTimeInterval = .seconds(Int(delay))
    DispatchQueue.main.asyncAfter(deadline: .now() + additionalTime, execute: clause)
}

func da_main(block: @escaping () -> Void) {
    if Thread.isMainThread {
        block()
    }
    else {
        DispatchQueue.main.async(execute: block)
    }
}

func da_main_async(block: @escaping () -> Void) {
    DispatchQueue.main.async(execute: block)
}

func da_default(block: @escaping () -> Void) {
    DispatchQueue.global().async(execute: block)
}

func da_default(block: @escaping () -> Void, after second: Double) {
    let task = DispatchWorkItem.init(block: block)
    taskDelay.add(task)
    DispatchQueue.global().asyncAfter(deadline: .now() + .milliseconds(Int(second * 1000))) {
        if !task.isCancelled {
            task.perform()
        }
        
        if taskDelay.contains(task) {
            taskDelay.remove(task)
        }
    }
}

func da_main_after(_ action: @escaping () -> Void, after second: Double) {
    let task = DispatchWorkItem.init(block: action)
    taskDelay.add(task)
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(second * 1000))) {
        if !task.isCancelled {
            task.perform()
        }
        
        if taskDelay.contains(task) {
            taskDelay.remove(task)
        }
    }
}

func da_main_after(_ action: @escaping () -> Void, afterMilisecond milisecond: Int) {
    let task = DispatchWorkItem.init(block: action)
    taskDelay.add(task)
    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(milisecond)) {
        if !task.isCancelled {
            task.perform()
        }
        
        if taskDelay.contains(task) {
            taskDelay.remove(task)
        }
    }
}

func cancelAllTaskDelayIfNeeded() {
    taskDelay.forEach { (item) in
        if let task = item as? DispatchWorkItem {
            task.cancel()
        }
    }
    
    taskDelay.removeAllObjects()
}

func da_global_high(block: @escaping () -> Void) {
    DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async(execute: block)
}

func fLog(_ message: String, _ args: CVarArg...,
    function: String = #function,
    file: String = #file,
    line: Int = #line) {
    let fileUrl = NSURL.init(fileURLWithPath: file)
    NSLog("<\(fileUrl.lastPathComponent ?? ""):\(function):\(line))> \(message)", args)
}

// MARK: - Utils class

class Utils {
    // MARK: Global Boolean
    static let isIPad : Bool = UIDevice.current.userInterfaceIdiom == .pad
    static let isIPhone5AndBelow: Bool = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height <= 568
    static let isIPhone4 = (UIScreen.main.bounds.height == 480.0)
    static let isIPhone5 = (UIScreen.main.bounds.height == 568.0)
    static let isIPhone6 = (UIScreen.main.bounds.height == 667.0)
    static let isIPhone6Plus = (UIScreen.main.bounds.height == 736.0)
    static var isOnDarkMode: Bool {
        if #available(iOS 13.0, *), UIScreen.main.traitCollection.userInterfaceStyle == .dark {
            return true
        }
        return false
    }
    
    //Standard keyboard height
    static let iPhoneKeyboardHeight: CGFloat = 224.0
    static func keyboardHeightWithNotification(_ notification: NSNotification, onView view: UIView? = nil, animationDuration: inout Double, animationCurve: inout UIView.AnimationOptions) -> CGFloat {
        let userInfo = notification.userInfo!
    
        animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let rawAnimationCurve = (notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        animationCurve = UIView.AnimationOptions.init(rawValue: UInt(rawAnimationCurve))
        
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if let view = view {
            let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
            let bottomMargin = view.bounds.maxY - convertedKeyboardEndFrame.minY
            return bottomMargin
        }
        else {
            return keyboardEndFrame.size.height
        }
    }
    
    // MARK: Global Methods
    
    enum Typeface: String {
        case Regular = "Roboto-Regular"
        case Thin = "Roboto-Thin"
        case Medium = "Roboto-Medium"
        case Bold = "Roboto-Bold"
        case Light = "Roboto-Light"
    }
    
    class func getCustomizeFont(_ fontSize: CGFloat, type: Typeface) -> UIFont {
        return UIFont(name: type.rawValue, size: fontSize)!
    }

    class func getCustomizeFont(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: fontSize)!
    }
    
    class func colorWithHex(_ hex: Int) -> UIColor {
        return UIColor(hex: hex)
    }
    
    class func colorWithHex8(_ hex8: UInt32) -> UIColor {
        return UIColor(hex8: hex8)
    }
    
    class func imageWithImage(_ image: UIImage, newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x:0, y:0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x:0, y:0, width: 1, height: 1);
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    class func generateQRCode(from string: String, width: CGFloat, height: CGFloat) -> UIImage? {
        let data = string.data(using: .isoLatin1)
        
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            
            guard let qrCodeImage = filter.outputImage else { return nil }
            
            let scaleX = width / qrCodeImage.extent.size.width
            let scaleY = height / qrCodeImage.extent.size.height
            let transform = CGAffineTransform.init(scaleX: scaleX, y: scaleY)
            
            let output = qrCodeImage.transformed(by: transform)
            
            return UIImage.init(ciImage: output)
        }
        
        return nil
    }
    
    // MARK: Wifi support
    
    class func getWiFiSSID() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
    
    class func getWiFiBSSID() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeyBSSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
    
    // MARK: Validate data
    
    class func isValidEmail(email: String) -> Bool {
        var isValid = true
        let emailRegEx = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let emailString = email as NSString
            let results = regex.matches(in: email, range: NSRange(location: 0, length: emailString.length))
            if results.count == 0 {
                isValid = false
            }
        } catch let error as NSError {
            print("Invalid email regex: \(error.localizedDescription)")
            isValid = false
        }
        return  isValid
    }
    
    class func isDigitsOnly(string: String) -> Bool {
        var isValid = true
        let digitRegEx = "[0-9]+"
        do {
            let regex = try NSRegularExpression(pattern: digitRegEx)
            let digitString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: digitString.length))
            if results.count == 0 {
                isValid = false
            }
        } catch let error as NSError {
            print("Invalid digit regex: \(error.localizedDescription)")
            isValid = false
        }
        return  isValid
    }
    
    class func isMatchedPattern(source: String?, pattern: String!) -> Bool {
        var isValid = false
        do {
            let regex = try NSRegularExpression(pattern: pattern)
            if let srcStr = source {
                let results = regex.matches(in: srcStr, range: NSRange(location: 0, length: srcStr.lengthOfBytes(using: .utf8)))
                if results.count > 0 {
                    isValid = true
                }
            }
        } catch let error as NSError {
            print("String '\(String(describing: source))' does not match '\(pattern ?? "")' regex: \(error.localizedDescription)")
        }
        return  isValid
    }
    
    class func isStringIsEmpty(inputStr: String?) -> Bool {
        return inputStr == nil || inputStr!.isEmpty || inputStr! == ""
    }
    
    class func stringFromHTML(string: String?) -> String {
        do {
            let str = try NSAttributedString(data:string!.data(using: String.Encoding.utf8, allowLossyConversion: true
                )!, options:[.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            return str.string
        } catch {
            print("HTML error:",error)
        }
        return ""
    }
    
    class func joinStringWithDelimiter(delimiter: String, listValue: [String]) -> String {
        var result = ""
        for value in listValue {
            if listValue.firstIndex(of: value) == listValue.count - 1 {
                result.append("\(value)")
            } else {
                result.append("\(value)\(delimiter)")
            }
        }
        return result
    }
    
    // MARK: Converter
    
    class func convertCelsiusToFahrenheit(celsius: Double) -> Double {
        return (celsius - 32) * 5 / 9
    }
    
    class func convertHexStringToInt(hexString: String) -> Int {
        var uint8Pointer : UnsafeMutablePointer<Int8>?
        let result = strtol(hexString, &uint8Pointer, 16)
        return result
    }
    
    /**
     * Binary check for ZWave
     *
     * @param int1
     * @return
     */
    class func toBinaryCheck(intNumber: Int) -> [Int] {
        
        var intNum = intNumber
        var data: [Int] = []
        
        let intLength = MemoryLayout<Int>.size * 8
        
        var i = intLength - 1
        while (i >= 0) {
            
            if (intNum % 2 == 1) {
                data.append(intLength - i)
            }
            intNum = intNum >> 1
            
            i -= 1
        }

        return data;
    }
    
    /**
     * Boolean array to Integer
     *
     * @param values
     * @return
     */
    public static func booleanArrayToInteger(values: [Bool]) -> Int {
        var n: Int = 0
        for b in values {
            n = (n << 1) | (b ? 1 : 0)
        }
        
        return n;
    }
    
    /**
     * Get highest security key for ZWave
     *
     * @param key
     * @return
     */
    public static func getHighestSecurityKey(withKey key: Int) -> Int {
        var data = toBinaryCheck(intNumber: key)
        
        let removedValues = [8, 10]
        
        for rmVal in removedValues {
            if data.count > 0 {
                for rmIdx in 0...data.count - 1 {
                    if rmVal == data[rmIdx] {
                        data.remove(at: rmIdx)
                        break
                    }
                }
            }
        }
        
        if data.count > 0 {
            return data.last!
        }
        
        return 0
    }
    
    public static func intToStringSize3(s2Scheme: Int) ->String {
        var result = "\(s2Scheme)"
        while (result.count < 3) {
            result = "0" + result
        }
        return result;
    }
    
    /**
     * Reversed function of hex2BytesToInt5Digits()
     *
     * @param input
     * @param inputLength
     * @param bitLength
     * @return
     */
    public static func decode5DigitsTo2Bytes(input: String, inputLength: Int, bitLength: Int) -> String? {
        let block = 5
        var hex = ""
        if Utils.isMatchedPattern(source: input, pattern: "\\d+") && input.count >= inputLength {
            var digits: [String] = []
            var index = 0
            while (index < input.count) {
                let minIndex = (index + block) < input.count ? (index + block) : input.count
                digits.append(input[index...minIndex - index])
//                digits.append(input.substring(with: Range.init(NSMakeRange(index, minIndex - index), in: input)!)) //(input.substring(index, Math.min(index + block, input.count())));
                index += block
            }
            if (digits.count >= inputLength / block) {
                for i in 0...(inputLength / block) - 1 { //(int i = 0; i < inputLength / block; i++) {
                    hex = hex + integerToHexString(value: Int.init(digits[i])!, bit: bitLength);
                }
                return hex
            }
        }
        return hex
    }
    
    /**
     * Integer to hex string
     */
    public static func integerToHexString(value: Int, bit: Int) -> String{
        if bit <= 0 {
            return ""
        }
        var hex = String.init(format: "%.0\(bit)X", value)
        if (hex.count > bit) {
            hex = hex.substringFromIndex(index: hex.count - bit)
        }
        return hex
    }
    
    public static func intToOxHexString(value: Int, bit: Int) -> String {
        if bit <= 0 {
            return ""
        }
        
        let hex = "0x" + String.init(format: "%.0\(bit*2)X", value)
        return hex
    }
    
    static func integerToSigned(_ value: Int, _ length: Int) -> Int {
        if value <= 0 {
            return 0
        }
        switch length {
        case 1:
            return Int(Int8(bitPattern: UInt8(value)))
        case 2:
            return Int(Int16(bitPattern: UInt16(value)))
        default:
            return value
        }
    }
    
    static func integerToUnsigned(_ value: Int, _ length: Int) -> Int {
        switch length {
        case 1:
            return Int(UInt8(bitPattern: Int8(value)) & 0xFF)
        case 2:
            return Int(UInt16(bitPattern: Int16(value)) & 0xFFFF)
        default:
            return value
        }
    }

    /**
     * Convert 2 bytes (4 digit) into 5 integer decimal
     *
     * @param hex
     * @return
     */
    public static func hex2BytesToInt5Digits(_ hexString: String?) -> String? {
        
        guard var hex = hexString, hex.count > 0 else { return nil}
        
        if (hex.count > 4) {
            hex = hex.substringToIndex(index: 4) //hex.substring(0, 4);
        } else if (hex.count < 4) {
            while (hex.count < 4) {
                hex = "0" + hex
            }
        }
        let pattern = "([0-9a-fA-F]{4})"
        if Utils.isMatchedPattern(source: hex, pattern: pattern) {
            let int = strtoul(hex, nil, 16)
            var fiveDigitStr = String.init(format: "%.05d", int)
            
            if fiveDigitStr.count > 5 {
                fiveDigitStr = fiveDigitStr.substringFromIndex(index: fiveDigitStr.count - 5)
            }
            
            return fiveDigitStr
        }
        return nil;
    }
    
    public static func hexToAsciiString(hexString: String) -> String {
        if (hexString.count % 2 != 0) {
            return ""
        }
        
        var result = ""
        var i = 0
        while i < hexString.count {
            
            let hex = hexString[i...2]
//            let hex = hexString.substring(with: Range.init(NSMakeRange(i, 2), in: hexString)!)
            let int = strtol(hex, nil, 16)
            //let int8 = Int8.init(int)
            var char = Character.init("")
            if let charCode = Unicode.Scalar.init(int) {
                char = Character.init(charCode)
            }
            
            result.append(char)
            
            i += 2
        }

        return result
    }
    
    /**
     * convert string to mac address
     */
    public static func convertStringToMacAddress(_ mac: String) -> String {
        if (mac.count != 12) {
            return ""
        }
        return mac.replaceRegexMatches(pattern: "([0-9A-Fa-f]{2})", replaceWith: "$1:").substringToIndex(index: 17)
    }
    
    static func stringClassFromString(_ className: String) -> AnyClass? {
        if let namespace = Bundle.main.infoDictionary?["CFBundleExecutable"] as? String, let cls = NSClassFromString("\(namespace).\(className)") {
            return cls
        }
        
        return nil
    }
    
    static func getAppVersionInfo() -> String {
        let infoDictionary = Bundle.main.infoDictionary
        let appName = infoDictionary?["CFBundleDisplayName"] ?? ""
        let appVersion = infoDictionary?["CFBundleShortVersionString"] ?? ""
        let appBuild = infoDictionary?[String(kCFBundleVersionKey)] ?? ""
        let device = UIDevice.current

        var appVersionString = "\(appName)_\(appVersion)(\(appBuild)) \(device.name), \(device.model)_\(device.systemName).\(device.systemVersion)"
        
        #if ADHOC
        appVersionString = appVersionString + "-" + "AdHoc"
        #endif
        
        #if STAGING
        appVersionString = appVersionString + "-" + "Staging"
        #endif
        
        return appVersionString
    }

    static func macAddress(fromLocalId: String) -> String {
        guard fromLocalId.count >= (2 + 12) else {
            return ""
        }
        var mac = fromLocalId.substringFromIndex(index: 2).uppercased()
        var macWithColon = mac.substringToIndex(index: 2)
        mac = mac.substringFromIndex(index: 2)
        
        for _ in 1..<6 {
            macWithColon = "\(macWithColon):\(mac.substringToIndex(index: 2))"
            mac = mac.substringFromIndex(index: 2)
        }
        
        return macWithColon
    }
    
    static func gotoWifiSetting() {
        if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl)
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
    
    static func secondsToDisplayString(seconds: Int) -> String {
        let hours = seconds / 3600
        let remaining = seconds % 3600
        
        let mins = remaining / 60
        let secs = remaining % 60
        
        var hoursString = ""
        if hours >= 10 {
            hoursString = "\(hours):"
        }
        else if hours > 0 {
            hoursString = "0\(hours):"
        }
        
        var minsString = "00:"
        if mins >= 10 {
            minsString = "\(mins):"
        }
        else if mins > 0 {
            minsString = "0\(mins):"
        }
        
        var secsString = "00"
        if secs >= 10 {
            secsString = "\(secs)"
        }
        else if secs > 0 {
            secsString = "0\(secs)"
        }
        
        return "\(hoursString)\(minsString)\(secsString)"
    }
}
