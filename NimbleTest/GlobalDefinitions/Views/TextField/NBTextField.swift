//
//  NBTextField.swift
//  NimbleTest
//
//  Created by Tran Kien Nghi on 7/31/20.
//  Copyright © 2020 Company. All rights reserved.
//

import UIKit

class NBTextField: UITextField {
    var padding: CGFloat = 12
    
    var isPasswordField: Bool = false {
        didSet {
            if isPasswordField == true {
                let showPasswordBtn = UIButton(type: .custom)
                showPasswordBtn.setImage(UIImage(named: "ic-eye"), for: .normal)
                showPasswordBtn.setImage(UIImage(named: "ic-eye-slash"), for: .selected)
                showPasswordBtn.contentEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
                showPasswordBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
                showPasswordBtn.backgroundColor = .clear
                showPasswordBtn.addTarget(self, action: #selector(showPasswordButtonTapped(sender:)), for: .touchUpInside)
                showPasswordBtn.isSelected = false
                self.rightView = showPasswordBtn
                self.rightViewMode = .always
            }
            else {
                self.rightView = nil
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.layer.borderColor = UIColor.lcBlack20.cgColor
        self.layer.borderWidth = CGFloat(1.0)
        self.layer.cornerRadius = 10.0
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + padding, y: bounds.origin.y + padding,
                      width: bounds.size.width - (padding * 2), height: bounds.size.height - (padding * 2))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    @objc func showPasswordButtonTapped(sender: Any) {
        if let showPasswordBtn = sender as? UIButton {
            showPasswordBtn.isSelected = !showPasswordBtn.isSelected
            self.isSecureTextEntry = !showPasswordBtn.isSelected
        }
    }
    
    //MARK: - Keyboard language
    func tryLoggingPrimaryLanguageInfoOnKeyboard() {
        print("Total number of keyboards. : \(UITextInputMode.activeInputModes.count)")
        for keyboardInputModes in UITextInputMode.activeInputModes{
            if let language = keyboardInputModes.primaryLanguage{
               dump(language)
            }
        }
    }
    
    var languageCode: String?{
        didSet {
            if self.isFirstResponder{
                self.resignFirstResponder();
                self.becomeFirstResponder();
            }
        }
    }
    
    override var textInputMode: UITextInputMode?{
        print("Total number of keyboards. : \(UITextInputMode.activeInputModes.count)")
        
        if let languageCode = self.languageCode {
            for keyboardInputModes in UITextInputMode.activeInputModes {
                if let language = keyboardInputModes.primaryLanguage {
                    if language == languageCode {
                        print("Set keyboard to \(language) when entering \(self.placeholder ?? "")")
                        return keyboardInputModes;
                    }
                }
            }
        }
        print("failed")
        return super.textInputMode;
    }
}
