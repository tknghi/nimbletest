//
//  NimbleTestTests.swift
//  NimbleTestTests
//
//  Created by Nghi Tran on 11/30/20.
//

import XCTest
@testable import NimbleTest

class NimbleTestTests: XCTestCase {

    override class func setUp() {
        NBKeychain.accessTokenString = "wfVsVZyUO4ryCCCLYlVdb5hrVQd0ABzrNHXV6X4MXac"
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLogin() throws {
        let accountViewModel = NBAccountSigninViewModel()
        
        accountViewModel.signIn.apply(("tknghi@gmail.com", "12345678", CloudAPIManager.provider)).startWithResult { (result) in
            switch result {
            case .success(_):
                XCTAssert(true)
            case .failure(_):
                XCTAssert(false)
            }
        }
    }
    
    func testLoadAccount() throws {
        let accountViewModel = NBAccountSigninViewModel()
        
        accountViewModel.loadAccount.apply((CloudAPIManager.provider)).startWithResult { (result) in
            switch result {
            case .success(let user):
                if user.email == "tknghi@gmail.com" {
                    XCTAssert(true)
                }
            case .failure(_):
                break
            }
            
            XCTAssert(false)
        }
    }
    
    func testFreshToken() throws {
        let accountViewModel = NBAccountSigninViewModel()
        
        NBKeychain.accessTokenString = "wfVsVZyUO4ryCCCLYlVdb5hrVQd0ABzrNHXV6X4MXac" + "HACK"
        
        accountViewModel.loadAccount.apply((CloudAPIManager.provider)).startWithResult { (result) in
            switch result {
            case .success(let user):
                if user.email == "tknghi@gmail.com" {
                    XCTAssert(true)
                }
            case .failure(_):
                break
            }
            
            XCTAssert(false)
        }
    }


    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
